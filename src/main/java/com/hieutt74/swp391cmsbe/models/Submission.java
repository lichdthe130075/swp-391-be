package com.hieutt74.swp391cmsbe.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "submissions")
@Data
@NoArgsConstructor
public class Submission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "assignment_id", nullable = false)
    private Assignment assignment;

    @ManyToOne
    @JoinColumn(name = "student_id", nullable = false)
    private UserEntity user;

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private LocalDateTime submissionDate;

    @Lob
    @Size(max = 500)
    private String studentNote;

    @Lob
    @NotNull
    @Size(max = 2048)  // Assuming the URL length won't exceed 2048 characters
    @Pattern(regexp = "^(http|https)://.*$", message = "Invalid URL format")
    private String uploadFile;

    private Integer status;

    private BigDecimal grade;

    @Lob
    @Size(max = 500)
    private String comment;

}
