package com.hieutt74.swp391cmsbe.security;

public class SecurityConstants {
    public static final long JWT_EXPERATION =  40 * 60 * 1000;
    public static final String JWT_SECRET = "secret";
}
