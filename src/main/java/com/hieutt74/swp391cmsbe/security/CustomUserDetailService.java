package com.hieutt74.swp391cmsbe.security;

import com.hieutt74.swp391cmsbe.models.Role;
import com.hieutt74.swp391cmsbe.models.UserEntity;
import com.hieutt74.swp391cmsbe.repository.AuthRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;

@Service
public class CustomUserDetailService implements UserDetailsService{

    private AuthRepository authRepository;
    @Autowired
    public CustomUserDetailService(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = authRepository.findByUsername(username).orElseThrow(()-> new  UsernameNotFoundException("User not found"));
        System.out.println("test user " + mapRoleToAuthorities(user.getRole()).stream().toString());
        return new User(user.getUsername(),user.getPassword(),mapRoleToAuthorities(user.getRole()));
    }

    private Collection<GrantedAuthority> mapRoleToAuthorities(Role role){
        GrantedAuthority authority = new SimpleGrantedAuthority(role.getRoleName());
        // Return a collection containing the single authority
        return Collections.singletonList(authority);

    }
}
