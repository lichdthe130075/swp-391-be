package com.hieutt74.swp391cmsbe.ultils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Service
@PropertySource("classpath:messages_exception.properties")
public class MessageException {

    //-------------- role --------------
    @Value("${messages.exception.role-not-found}")
    public String MSG_ROLE_NOT_FOUND;


}
