package com.hieutt74.swp391cmsbe.ultils;

import com.hieutt74.swp391cmsbe.dto.request.RegisterDTO;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ExcelService {

    public List<RegisterDTO> parseExcelFile(MultipartFile file) throws IOException {
        List<RegisterDTO> userList = new ArrayList<>();
        Workbook workbook = WorkbookFactory.create(file.getInputStream());
        Sheet sheet = workbook.getSheetAt(0);

        for (Row row : sheet) {
            if (row.getRowNum() == 0) {
                continue; // Skip header row
            }
            RegisterDTO user = new RegisterDTO();
            user.setFirstName(getCellValueAsString(row.getCell(0)));
            user.setLastName(getCellValueAsString(row.getCell(1)));
            user.setPhoneNumber(getCellValueAsString(row.getCell(2)));
            user.setEmail(getCellValueAsString(row.getCell(3)));
            user.setAddress(getCellValueAsString(row.getCell(4)));
            user.setRole((int) getCellValueAsNumeric(row.getCell(5))); // Assuming role is in the 6th column
            userList.add(user);
        }

        workbook.close();
        return userList;
    }

    private String getCellValueAsString(Cell cell) {
        if (cell == null) {
            return "";
        }

        switch (cell.getCellType()) {
            case STRING:
                return cell.getStringCellValue();
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    return cell.getDateCellValue().toString();
                } else {
                    return String.valueOf((int) cell.getNumericCellValue());
                }
            case BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            case FORMULA:
                return cell.getCellFormula();
            default:
                return "";
        }
    }

    private double getCellValueAsNumeric(Cell cell) {
        if (cell == null) {
            return 0;
        }

        switch (cell.getCellType()) {
            case NUMERIC:
                return cell.getNumericCellValue();
            case STRING:
                try {
                    return Double.parseDouble(cell.getStringCellValue());
                } catch (NumberFormatException e) {
                    return 0;
                }
            case BOOLEAN:
                return cell.getBooleanCellValue() ? 1 : 0;
            case FORMULA:
                try {
                    return cell.getNumericCellValue();
                } catch (Exception e) {
                    return 0;
                }
            default:
                return 0;
        }
    }
}
