package com.hieutt74.swp391cmsbe.ultils;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class StringToArrayList {

    public static List<String> stringProcess(String jsonString) {
//        String jsonString = "[\"hello\",\"bcd\",\"giveaway\"]";
        Gson gson = new Gson();
        // Convert the JSON string to an array of Strings
        String[] stringArray = gson.fromJson(jsonString, String[].class);
        // Convert the String array to an ArrayList
        List<String> stringList = new ArrayList<>(List.of(stringArray));
       return stringList;
    }
}
