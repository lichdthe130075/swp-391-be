package com.hieutt74.swp391cmsbe.ultils;

import org.springframework.stereotype.Service;

@Service
public class VietnameseToneRemover {

    public String removeVietnameseTones(String text) {
        if (text == null) {
            return "";
        }
        String str = text;
        str = str.replaceAll("[àáạảãâầấậẩẫăằắặẳẵ]", "a");
        str = str.replaceAll("[èéẹẻẽêềếệểễ]", "e");
        str = str.replaceAll("[ìíịỉĩ]", "i");
        str = str.replaceAll("[òóọỏõôồốộổỗơờớợởỡ]", "o");
        str = str.replaceAll("[ùúụủũưừứựửữ]", "u");
        str = str.replaceAll("[ỳýỵỷỹ]", "y");
        str = str.replaceAll("[đ]", "d");
        str = str.replaceAll("[ÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴ]", "A");
        str = str.replaceAll("[ÈÉẸẺẼÊỀẾỆỂỄ]", "E");
        str = str.replaceAll("[ÌÍỊỈĨ]", "I");
        str = str.replaceAll("[ÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠ]", "O");
        str = str.replaceAll("[ÙÚỤỦŨƯỪỨỰỬỮ]", "U");
        str = str.replaceAll("[ỲÝỴỶỸ]", "Y");
        str = str.replaceAll("[Đ]", "D");
        // Remove combining accents
        str = str.replaceAll("[\\u0300\\u0301\\u0303\\u0309\\u0323]", "");
        str = str.replaceAll("[\\u02C6\\u0306\\u031B]", "");
//        // Remove extra spaces
//        str = str.replaceAll(" +", " ");
//        str = str.trim();
//        // Remove punctuations
//        str = str.replaceAll("[!@%\\^\\*\\(\\)\\+\\=<>\\?/,.\\:\"'&#\\[\\]~\\$`_-{|}\\\\]", " ");
        System.out.println(str);
        return str;
    }

}
