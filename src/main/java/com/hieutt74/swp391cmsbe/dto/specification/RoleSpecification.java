package com.hieutt74.swp391cmsbe.dto.specification;

import com.hieutt74.swp391cmsbe.dto.request.RoleDTO;
import com.hieutt74.swp391cmsbe.dto.request.UserDTO;
import com.hieutt74.swp391cmsbe.models.Role;
import com.hieutt74.swp391cmsbe.models.UserEntity;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public class RoleSpecification {

    private Specification<Role> buildStringSpecification(String value, String fieldName) {
        return (root, query, builder) -> builder.like(root.get(fieldName), "%" + value + "%");
    }

    public Specification<Role> createSpecification(RoleDTO roleDTO) {
        Specification<Role> specification = Specification.where(null);

        if (roleDTO != null) {
            if (roleDTO.getRoleName() != null) {
                specification = specification.and(buildStringSpecification(roleDTO.getRoleName(), "roleName"));
            }

            // Additional filters can be added here similarly
        }

        return specification;
    }
}
