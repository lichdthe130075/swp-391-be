package com.hieutt74.swp391cmsbe.dto.specification;

import com.hieutt74.swp391cmsbe.dto.request.CourseDTO;
import com.hieutt74.swp391cmsbe.models.Course;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public class CourseSpecification {

    private Specification<Course> buildStringSpecification(String value, String fieldName) {
        return (root, query, builder) -> builder.like(root.get(fieldName), "%" + value + "%");
    }

    private Specification<Course> buildNumericSpecification(Number value, String fieldName) {
        return (root, query, builder) -> builder.equal(root.get(fieldName), value);
    }

    public Specification<Course> createSpecification(CourseDTO courseParams) {
        System.out.println(courseParams);
        Specification<Course> specification = Specification.where(null);

        if (courseParams != null) {
            if (courseParams.getCourseCode() != null) {
                specification = specification.and(buildStringSpecification(courseParams.getCourseCode(), "courseCode"));
            }
            if (courseParams.getCourseName() != null) {
                specification = specification.and(buildStringSpecification(courseParams.getCourseName(), "courseName"));
            }
            if (courseParams.getCreatedBy() != null) {
                specification = specification.and(buildNumericSpecification(courseParams.getCreatedBy(), "createdBy"));
            }
            if (courseParams.getStatus() != null) {
                specification = specification.and(buildNumericSpecification(courseParams.getStatus(), "status"));
            }
            // Additional filters can be added here similarly
        }

        return specification;
    }
}
