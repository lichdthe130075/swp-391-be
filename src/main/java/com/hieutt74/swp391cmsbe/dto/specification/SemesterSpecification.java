package com.hieutt74.swp391cmsbe.dto.specification;

import com.hieutt74.swp391cmsbe.dto.request.ClassesDTO;
import com.hieutt74.swp391cmsbe.dto.request.SemesterDTO;
import com.hieutt74.swp391cmsbe.models.Classes;
import com.hieutt74.swp391cmsbe.models.Semester;
import org.springframework.data.jpa.domain.Specification;

public class SemesterSpecification {
    private Specification<Semester> buildStringSpecification(String value, String fieldName) {
        return (root, query, builder) -> builder.like(root.get(fieldName), "%" + value + "%");
    }

    private Specification<Semester> buildNumericSpecification(Number value, String fieldName) {
        return (root, query, builder) -> builder.equal(root.get(fieldName), value);
    }

    public Specification<Semester> createSpecification(SemesterDTO semesterParams) {
        Specification<Semester> specification = Specification.where(null);
        if (semesterParams != null) {
            if (semesterParams.getSemesterName() != null) {
                specification = specification.and(buildStringSpecification(semesterParams.getSemesterName(), "semester_name"));
            }


            // Additional filters can be added here similarly
        }

        return specification;
    }
}
