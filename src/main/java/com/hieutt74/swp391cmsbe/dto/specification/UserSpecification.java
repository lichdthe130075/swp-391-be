package com.hieutt74.swp391cmsbe.dto.specification;

import com.hieutt74.swp391cmsbe.dto.request.UserDTO;
import com.hieutt74.swp391cmsbe.models.UserEntity;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public class UserSpecification {

    private Specification<UserEntity> buildStringSpecification(String value, String fieldName) {
        return (root, query, builder) -> builder.like(root.get(fieldName), "%" + value + "%");
    }

    private Specification<UserEntity> buildNumericSpecification(Number value, String fieldName) {
        return (root, query, builder) -> builder.equal(root.get(fieldName), value);
    }

    private Specification<UserEntity> buildRoleSpecification(Integer roleId) {
        return (root, query, builder) -> builder.equal(root.join("role").get("id"), roleId);
    }

    public Specification<UserEntity> createSpecification(UserDTO userParam) {
        Specification<UserEntity> specification = Specification.where(null);

        if (userParam != null) {
            if (userParam.getUsername() != null) {
                specification = specification.and(buildStringSpecification(userParam.getUsername(), "username"));
            }
            if (userParam.getRole() != null) {
                specification = specification.and(buildRoleSpecification(userParam.getRole()));
            }
            if (userParam.getStatus() != null) {
                specification = specification.and(buildNumericSpecification(userParam.getStatus(), "status"));
            }
            // Additional filters can be added here similarly
        }

        return specification;
    }
}
