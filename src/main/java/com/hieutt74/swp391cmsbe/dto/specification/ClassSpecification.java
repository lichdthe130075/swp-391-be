package com.hieutt74.swp391cmsbe.dto.specification;

import com.hieutt74.swp391cmsbe.dto.request.ClassCourseDTO;
import com.hieutt74.swp391cmsbe.dto.request.ClassesDTO;
import com.hieutt74.swp391cmsbe.models.Classes;
import org.springframework.data.jpa.domain.Specification;

public class ClassSpecification {
    private Specification<Classes> buildStringSpecification(String value, String fieldName) {
        return (root, query, builder) -> builder.like(root.get(fieldName), "%" + value + "%");
    }

    private Specification<Classes> buildNumericSpecification(Number value, String fieldName) {
        return (root, query, builder) -> builder.equal(root.get(fieldName), value);
    }

    public Specification<Classes> createSpecification(ClassesDTO classParams) {
        Specification<Classes> specification = Specification.where(null);
        if (classParams != null) {
            if (classParams.getClassCode() != null) {
                specification = specification.and(buildStringSpecification(classParams.getClassCode(), "class_code"));
            }
            if (classParams.getMajorId() != null) {
                specification = specification.and(buildNumericSpecification(classParams.getMajorId(), "major_id"));
            }
            if (classParams.getSubMajorId() != null) {
                specification = specification.and(buildNumericSpecification(classParams.getSubMajorId(), "sub_major_id"));
            }

            // Additional filters can be added here similarly
        }

        return specification;
    }
}
