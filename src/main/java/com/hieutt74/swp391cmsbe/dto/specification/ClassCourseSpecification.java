package com.hieutt74.swp391cmsbe.dto.specification;

import com.hieutt74.swp391cmsbe.dto.request.ClassCourseDTO;
import com.hieutt74.swp391cmsbe.dto.request.CourseDTO;
import com.hieutt74.swp391cmsbe.models.ClassCourse;
import com.hieutt74.swp391cmsbe.models.Course;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component

public class ClassCourseSpecification {

    private Specification<ClassCourse> buildStringSpecification(String value, String fieldName) {
        return (root, query, builder) -> builder.like(root.get(fieldName), "%" + value + "%");
    }

    private Specification<ClassCourse> buildNumericSpecification(Number value, String fieldName) {
        return (root, query, builder) -> builder.equal(root.get(fieldName), value);
    }

    private Specification<ClassCourse> buildNumericIDSpecification(Number value, String attributeName) {
        return (root, query, builder) -> builder.equal(root.get(attributeName).get("id"), value);
    }




    public Specification<ClassCourse> createSpecification(ClassCourseDTO classCourseParams) {
        Specification<ClassCourse> specification = Specification.where(null);

        if (classCourseParams != null) {
            if (classCourseParams.getClassId() != null) {
                specification = specification.and(buildNumericIDSpecification(classCourseParams.getClassId(), "classes"));
            }
            if (classCourseParams.getSemesterId() != null) {
                specification = specification.and(buildNumericIDSpecification(classCourseParams.getSemesterId(), "semester"));
            }
            if (classCourseParams.getCourseId() != null) {
                specification = specification.and(buildNumericIDSpecification(classCourseParams.getCourseId(), "course"));
            }
            if (classCourseParams.getInstructorId() != null) {
                specification = specification.and(buildNumericIDSpecification(classCourseParams.getInstructorId(), "instructor"));
            }
        }

        return specification;
    }

}
