package com.hieutt74.swp391cmsbe.dto.response;

import com.hieutt74.swp391cmsbe.dto.request.SectionDTO;
import jakarta.persistence.Column;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
@Data

public class CourseResponse {
    private Long id;
    private String courseCode;
    private String courseName;
    private String courseDescription;
    private Set<SectionResponse> sections = new HashSet<>();
    private UserResponse createdBy;
    private LocalDateTime createdDate;
    private UserResponse lastModifiedBy;
    private LocalDateTime lastModifiedDate;
    private Integer status;


    public CourseResponse(){

    }
    public CourseResponse(Long id, String courseCode,String courseName, String courseDescription, Integer status) {
        this.id = id;
        this.courseCode = courseCode;
        this.courseName = courseName;
        this.courseDescription = courseDescription;
        this.status = status;
    }



}
