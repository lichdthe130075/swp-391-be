package com.hieutt74.swp391cmsbe.dto.response;

import com.hieutt74.swp391cmsbe.models.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EnrollmentResponse {
    private Long id;
    private ClassCourseResponse classCourse;
    private UserResponse student;
    private Integer enrollmentStatus;
    private LocalDateTime enrollmentDate;
}
