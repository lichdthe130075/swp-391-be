package com.hieutt74.swp391cmsbe.dto.response;

import com.hieutt74.swp391cmsbe.models.*;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClassCourseResponse {
    private Long id;
    private Classes classes;
    private Semester semester;
    private CourseResponse course;
    private UserEntity instructor;
    private List<Enrollment> enrolledStudents;
}
