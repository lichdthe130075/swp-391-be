package com.hieutt74.swp391cmsbe.dto.response;

import lombok.Data;

@Data
public class RegisterResponse {
    private String username;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String address;

}
