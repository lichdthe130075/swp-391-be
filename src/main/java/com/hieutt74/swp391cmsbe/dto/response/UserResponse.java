package com.hieutt74.swp391cmsbe.dto.response;

import com.hieutt74.swp391cmsbe.models.Role;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Data
public class UserResponse {
    private Long id;
    private String username;
//    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String address;
    private Role role;
    private Integer status;

    public UserResponse() {
       
    }

    public UserResponse(String username, String firstName, String lastName) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
