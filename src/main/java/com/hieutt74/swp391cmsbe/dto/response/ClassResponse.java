package com.hieutt74.swp391cmsbe.dto.response;

import com.hieutt74.swp391cmsbe.models.Major;
import com.hieutt74.swp391cmsbe.models.Semester;
import com.hieutt74.swp391cmsbe.models.SubMajor;
import com.hieutt74.swp391cmsbe.models.UserEntity;
import jakarta.persistence.Column;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDateTime;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClassResponse {
    private Long id;
    private String classCode;
    private Major major;
    private SubMajor subMajor;
    private Semester semester;
    private UserEntity createdBy;
    private LocalDateTime createdDate;
    private UserEntity lastModifiedBy;
    private LocalDateTime lastModifiedDate;
}
