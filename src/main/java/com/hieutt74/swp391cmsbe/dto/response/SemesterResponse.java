package com.hieutt74.swp391cmsbe.dto.response;

import com.hieutt74.swp391cmsbe.models.UserEntity;
import lombok.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class SemesterResponse {
    private Long id;
    private String semesterName;
    private int year;
    private LocalDate startDate;
    private LocalDate endDate;
    private String semesterDescription;
    private UserEntity createdBy;
    private LocalDateTime createdDate;
    private UserEntity lastModifiedBy;
    private LocalDateTime lastModifiedDate;
}
