package com.hieutt74.swp391cmsbe.dto.response;

import lombok.Data;

@Data
public class SectionResponse {
    private Long id;
    private String sectionName;
    private String sectionDescription;
    private int sectionOrder;


    public SectionResponse(Long id, String sectionName, String sectionDescription, int sectionOrder) {
        this.id = id;
        this.sectionName = sectionName;
        this.sectionDescription = sectionDescription;
        this.sectionOrder = sectionOrder;
    }
}
