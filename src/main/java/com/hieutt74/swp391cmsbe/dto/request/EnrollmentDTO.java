package com.hieutt74.swp391cmsbe.dto.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class EnrollmentDTO {
    private Long classCourseId;
    private Long studentId;

}
