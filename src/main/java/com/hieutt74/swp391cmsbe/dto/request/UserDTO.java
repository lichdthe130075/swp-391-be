package com.hieutt74.swp391cmsbe.dto.request;

import com.hieutt74.swp391cmsbe.models.Role;
import lombok.Data;

@Data
public class UserDTO {
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String address;
    private Integer status;
    private Integer role;
}
