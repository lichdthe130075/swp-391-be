package com.hieutt74.swp391cmsbe.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleDTO {
    private Long roleId;
    private String roleDescription;
    private String roleName;

    public Long createBy;
    public Long updateBy;
    public String creatOn;
    public String updateOn;
}
