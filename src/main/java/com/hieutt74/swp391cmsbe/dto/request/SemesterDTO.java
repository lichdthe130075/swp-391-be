package com.hieutt74.swp391cmsbe.dto.request;

import com.hieutt74.swp391cmsbe.models.Classes;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.Set;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SemesterDTO {
    private String semesterName;
    private String startDate;
    private String endDate;
    private String semesterDescription;
    private Long createdBy;
    private Long lastModifiedBy;
}
