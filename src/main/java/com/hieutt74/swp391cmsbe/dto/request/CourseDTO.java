package com.hieutt74.swp391cmsbe.dto.request;

import com.hieutt74.swp391cmsbe.models.Section;
import lombok.Data;
import org.springframework.core.SpringVersion;

import java.util.HashSet;
import java.util.Set;
@Data
public class CourseDTO {
    private String courseCode;
    private String courseName;
    private String courseDescription;
    private Set<SectionDTO> sections = new HashSet<>();
    private Long createdBy;
    private Integer status;
    private String managerFeedback;
}
