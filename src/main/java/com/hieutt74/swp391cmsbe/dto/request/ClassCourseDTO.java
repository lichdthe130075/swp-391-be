package com.hieutt74.swp391cmsbe.dto.request;

import lombok.*;

import java.util.List;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClassCourseDTO {
    private Long classId;
    private Long semesterId;
    private Long courseId;
    private Long instructorId;
    private Long createdBy;
    private Long lastModifiedBy;

}
