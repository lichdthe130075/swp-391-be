package com.hieutt74.swp391cmsbe.dto.request;

import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MajorDTO {
    private String name;
    private String acronym;

    private String description;
    private Set<SubMajorDTO> subMajors = new HashSet<>();
    private Long managerId;
}
