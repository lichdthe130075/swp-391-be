package com.hieutt74.swp391cmsbe.dto.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClassesDTO {
    private Long id;
    private String classCode;
    private Long majorId;
    private Long subMajorId;
    private Long createdBy;
    private Long updatedBy;
}
