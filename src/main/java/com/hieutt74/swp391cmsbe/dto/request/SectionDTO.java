package com.hieutt74.swp391cmsbe.dto.request;

import lombok.Data;

@Data
public class SectionDTO {
    private Long id;
    private String sectionName;
    private String sectionDescription;
    private int orderIndex; // New field to store the order index
}
