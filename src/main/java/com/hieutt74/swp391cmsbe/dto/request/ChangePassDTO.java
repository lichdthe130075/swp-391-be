package com.hieutt74.swp391cmsbe.dto.request;

import lombok.Data;

@Data
public class ChangePassDTO {
    private String username;
    private String oldPassword;
    private String newPassword;
}
