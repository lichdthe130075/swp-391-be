package com.hieutt74.swp391cmsbe.dto.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SubMajorDTO {
    private String name;
    private String acronym;
    private String field;
    private Long managerId;
}
