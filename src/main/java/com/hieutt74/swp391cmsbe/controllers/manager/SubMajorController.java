package com.hieutt74.swp391cmsbe.controllers.manager;

import com.hieutt74.swp391cmsbe.dto.request.SubMajorDTO;
import com.hieutt74.swp391cmsbe.models.SubMajor;
import com.hieutt74.swp391cmsbe.service.MajorService;
import com.hieutt74.swp391cmsbe.service.SubMajorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/manager/sub-major")
@CrossOrigin(origins = "*")
public class SubMajorController {
    @Autowired
    private SubMajorService subMajorService;

//    @PostMapping("/create")
//    public ResponseEntity<SubMajor> createSubMajor(@RequestBody SubMajorDTO subMajorDTO) {
//        SubMajor createdSubMajor = subMajorService.createSubMajor(subMajorDTO);
//        return new ResponseEntity<>(createdSubMajor, HttpStatus.CREATED);
//    }
//
//    @PatchMapping("/update")
//    public ResponseEntity<SubMajorDTO> updateSubMajor(@RequestBody SubMajorDTO subMajorDTO) {
//        SubMajorDTO updatedSubMajor = subMajorService.updateSubMajor(subMajorDTO);
//        return new ResponseEntity<>(updatedSubMajor, HttpStatus.OK);
//    }
//
//    @DeleteMapping("/delete/{id}")
//    public ResponseEntity<Void> deleteSubMajor(@PathVariable Long id) {
//        subMajorService.deleteSubMajor(id);
//        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//    }
//
//    @GetMapping("/get/{id}")
//    public ResponseEntity<SubMajorDTO> getSubMajor(@PathVariable Long id) {
//        SubMajorDTO subMajorDTO = subMajorService.getSubMajorById(id);
//        return new ResponseEntity<>(subMajorDTO, HttpStatus.OK);
//    }
//
//    @GetMapping("/get-all")
//    public ResponseEntity<List<SubMajorDTO>> getAllSubMajors() {
//        List<SubMajorDTO> subMajorDTOs = subMajorService.getAllSubMajors();
//        return new ResponseEntity<>(subMajorDTOs, HttpStatus.OK);
//    }
}
