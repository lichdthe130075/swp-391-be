package com.hieutt74.swp391cmsbe.controllers.manager;

import com.hieutt74.swp391cmsbe.dto.request.ClassesDTO;
import com.hieutt74.swp391cmsbe.dto.request.CourseDTO;
import com.hieutt74.swp391cmsbe.dto.response.ClassResponse;
import com.hieutt74.swp391cmsbe.models.Classes;
import com.hieutt74.swp391cmsbe.models.Course;
import com.hieutt74.swp391cmsbe.service.ClassesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/manager/")
@CrossOrigin(origins = "*")
public class ClassesController {
    @Autowired
    private ClassesService classesService;


    @GetMapping("/get-all")
    public ResponseEntity<Page<Classes>> getAllClasses( @RequestParam(value = "page", defaultValue = "0") int page,
                                                           @RequestParam(value = "size", defaultValue = "10") int size, @RequestBody ClassesDTO classParams) {
        try {
            Page<Classes> classes = classesService.getAllClasses(classParams, page, size);
            return ResponseEntity.ok(classes);
        } catch (Exception e) {
            System.out.println(e);
            // Log the exception and return an appropriate error response
            return ResponseEntity.status(500).build();
        }
    }

    @PostMapping("class/create")
    public ResponseEntity<Classes> createClass(@RequestBody ClassesDTO classesDTO) {
        Classes createdClass = classesService.createClass(classesDTO);
        return new ResponseEntity<>(createdClass, HttpStatus.CREATED);
    }

    @GetMapping("class/get/{id}")
    public ResponseEntity<ClassResponse> getClass(@PathVariable Long id) {
        ClassResponse classResponse = classesService.getClassById(id);
        if (classResponse != null) {
            return ResponseEntity.ok(classResponse);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("class/update")
    public ResponseEntity<ClassesDTO> updateClass( @RequestBody ClassesDTO classesDTO) {
        Optional<ClassesDTO> updatedClass = classesService.updateClass(classesDTO);
        return updatedClass.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("class/delete/{id}")
    public ResponseEntity<Void> deleteClass(@PathVariable Long id) {
        classesService.deleteClass(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
