package com.hieutt74.swp391cmsbe.controllers.manager;


import com.hieutt74.swp391cmsbe.dto.request.CourseDTO;
import com.hieutt74.swp391cmsbe.dto.response.CourseResponse;
import com.hieutt74.swp391cmsbe.models.Course;
import com.hieutt74.swp391cmsbe.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/manager/")
@CrossOrigin(origins = "*")
public class CourseMController {

    @Autowired
    private CourseService courseService;

    @GetMapping("courses")
    public Page<Course> getAllCoursesManager(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size , @RequestBody CourseDTO courseParams) throws Exception {
        return courseService.getAllCourses(courseParams,page, size);
    }

    @PutMapping("course/change-status")
    public ResponseEntity<CourseResponse> changeStatusCourse(@RequestParam(value = "courseId") Long courseId, @RequestBody CourseDTO course) {
        CourseResponse updatedCourse = courseService.changeStatusCourse(courseId,course);
        return ResponseEntity.ok(updatedCourse);
    }

}
