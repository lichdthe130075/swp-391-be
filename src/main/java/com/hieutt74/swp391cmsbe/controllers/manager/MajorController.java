package com.hieutt74.swp391cmsbe.controllers.manager;

import com.hieutt74.swp391cmsbe.dto.request.MajorDTO;
import com.hieutt74.swp391cmsbe.models.Major;
import com.hieutt74.swp391cmsbe.service.ClassesService;
import com.hieutt74.swp391cmsbe.service.MajorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/manager/major")
@CrossOrigin(origins = "*")
public class MajorController {
    @Autowired
    private MajorService majorService;

//    @GetMapping("/get-all")
//    public ResponseEntity<List<MajorDTO>> getAllMajors() {
//        List<MajorDTO> majorDTOs = majorService.getAllMajors();
//        return new ResponseEntity<>(majorDTOs, HttpStatus.OK);
//    }
//    @GetMapping("/get/{id}")
//    public ResponseEntity<MajorDTO> getMajor(@PathVariable Long id) {
//        MajorDTO majorDTO = majorService.getMajorById(id);
//        return new ResponseEntity<>(majorDTO, HttpStatus.OK);
//    }


    @PostMapping("/create")
    public ResponseEntity<Major> createMajor(@RequestBody MajorDTO majorDTO) {
        Major createdMajor = majorService.createMajor(majorDTO);
        return new ResponseEntity<>(createdMajor, HttpStatus.CREATED);
    }

//    @PatchMapping("/update")
//    public ResponseEntity<MajorDTO> updateMajor(@RequestBody MajorDTO majorDTO) {
//        MajorDTO updatedMajor = majorService.updateMajor(majorDTO.getId(), majorDTO);
//        return new ResponseEntity<>(updatedMajor, HttpStatus.OK);
//    }

    // delete method using @RequestParam
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteMajor(@PathVariable Long id) {
        majorService.deleteMajor(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }



}
