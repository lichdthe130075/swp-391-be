package com.hieutt74.swp391cmsbe.controllers.manager;

import com.hieutt74.swp391cmsbe.dto.request.ClassesDTO;
import com.hieutt74.swp391cmsbe.dto.request.SemesterDTO;
import com.hieutt74.swp391cmsbe.dto.response.SemesterResponse;
import com.hieutt74.swp391cmsbe.models.Classes;
import com.hieutt74.swp391cmsbe.models.Semester;
import com.hieutt74.swp391cmsbe.service.SemesterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/manager/")
@CrossOrigin(origins = "*")
public class SemesterController {
    @Autowired
    private SemesterService semesterService;

    // create method
    @PostMapping("semester/create")
    public ResponseEntity<Semester> createSemester(@RequestBody SemesterDTO semesterDTO) {
        Semester createdSemester = semesterService.createSemester(semesterDTO);
        return new ResponseEntity<>(createdSemester, HttpStatus.CREATED);
    }

    // update method
    @PatchMapping("semester/update/{id}")
    public ResponseEntity<Semester> updateSemester(@PathVariable Long id, @RequestBody SemesterDTO semesterDTO) {
        Semester updatedSemester = semesterService.updateSemester(id,semesterDTO);
        return new ResponseEntity<>(updatedSemester, HttpStatus.OK);
    }

    // delete method
    @DeleteMapping("semester/delete/{id}")
    public ResponseEntity<Void> deleteSemester(@PathVariable Long id) {
        semesterService.deleteSemester(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // get method
    @GetMapping("semester/get/{id}")
    public ResponseEntity<SemesterResponse> getSemester(@PathVariable Long id) {
        SemesterResponse semesterResponse = semesterService.getSemesterById(id);
        return new ResponseEntity<>(semesterResponse, HttpStatus.OK);
    }

    // get all method
    @GetMapping("semester/get-all")
    public ResponseEntity<Page<Semester>> getAllSemesters(@RequestParam(value = "page", defaultValue = "0") int page,
                                                          @RequestParam(value = "size", defaultValue = "10") int size, @RequestBody SemesterDTO semesterParams) {
        try {
            Page<Semester> semesters = semesterService.listAllSemesters(semesterParams,page,size);
            return ResponseEntity.ok(semesters);
        } catch (Exception e) {
            System.out.println(e);
            // Log the exception and return an appropriate error response
            return ResponseEntity.status(500).build();
        }
    }
}
