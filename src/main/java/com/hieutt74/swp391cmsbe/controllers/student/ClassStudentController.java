package com.hieutt74.swp391cmsbe.controllers.student;


import com.hieutt74.swp391cmsbe.dto.request.ClassCourseDTO;
import com.hieutt74.swp391cmsbe.dto.response.ClassCourseResponse;
import com.hieutt74.swp391cmsbe.dto.response.EnrollmentResponse;
import com.hieutt74.swp391cmsbe.models.Enrollment;
import com.hieutt74.swp391cmsbe.service.ClassCourseService;
import com.hieutt74.swp391cmsbe.service.EnrollmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/api/student/")
@CrossOrigin(origins = "*")
public class ClassStudentController {

    @Autowired
    private ClassCourseService classCourseService;

    @Autowired
    private EnrollmentService enrollmentService;

    @PostMapping("class/search")
    public Page<ClassCourseResponse> studentSearch(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size,@RequestBody ClassCourseDTO classCourseParams) throws Exception {
        return classCourseService.studentSearchClass(page, size,classCourseParams);
//        return null;
    }

    @GetMapping("class/get-detail/{id}")
    public ClassCourseResponse getClassDetail(@PathVariable Long id) {
        try {
            return classCourseService.getClassCourseStudentById(id);
        }catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }


    @GetMapping("class/get-all-enrolled-classes/{studentId}")
    public ResponseEntity<List<EnrollmentResponse>> getAllEnrollmentByStudentId(@PathVariable Long studentId) {
        try {
            List<EnrollmentResponse> enrollments = enrollmentService.getAllEnrollmentByStudentId(studentId);
            return ResponseEntity.ok(enrollments);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @GetMapping("class/get-all-enrolled-classes-table/{studentId}")
    public ResponseEntity<Page<EnrollmentResponse>> getAllEnrollmentByStudentId(@PathVariable Long studentId,@RequestParam(value = "page", defaultValue = "0") int page,
                                                                                @RequestParam(value = "size", defaultValue = "10") int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<EnrollmentResponse> enrollments = enrollmentService.getAllEnrollmentByStudentId(studentId,pageable);
            return ResponseEntity.ok(enrollments);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }




    @GetMapping("class/get-detail-enrolled-classes/{enrollmentId}")
    public EnrollmentResponse getDetailEnrolledClass(@PathVariable Long enrollmentId) {
        try {
            return enrollmentService.getDetailEnrolledClass(enrollmentId);
        }catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }




}
