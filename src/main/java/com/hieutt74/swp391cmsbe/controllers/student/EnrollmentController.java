package com.hieutt74.swp391cmsbe.controllers.student;

import com.hieutt74.swp391cmsbe.dto.request.EnrollmentDTO;
import com.hieutt74.swp391cmsbe.dto.request.RoleDTO;
import com.hieutt74.swp391cmsbe.models.Enrollment;
import com.hieutt74.swp391cmsbe.service.EnrollmentService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/student/")
@CrossOrigin(origins = "*")
public class EnrollmentController {

    @Autowired
    private EnrollmentService enrollmentService;

    @PostMapping("enrollment")
    public ResponseEntity<Object> createEnrollment(@RequestBody EnrollmentDTO enrollmentDTO) {
        try {
            Enrollment createdEnrollment = enrollmentService.createEnrollment(enrollmentDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdEnrollment);
        } catch (IllegalArgumentException ex) {
            // Handle specific exception for invalid input
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("message", ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        } catch (EntityNotFoundException ex) {
            // Handle specific exception for not found entities
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("message", "User or ClassCourse not found");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
        } catch (Exception ex) {
            // Log the exception and return a generic error message
            ex.printStackTrace(); // Use a logger in a real-world application
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("message", "Failed to create enrollment");
            errorResponse.put("error", ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
        }
    }
}
