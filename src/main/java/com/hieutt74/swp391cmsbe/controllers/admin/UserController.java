package com.hieutt74.swp391cmsbe.controllers.admin;

import com.hieutt74.swp391cmsbe.dto.request.ChangePassDTO;
import com.hieutt74.swp391cmsbe.dto.request.UserDTO;
import com.hieutt74.swp391cmsbe.models.UserEntity;
import com.hieutt74.swp391cmsbe.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/user/")
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("search")
    public ResponseEntity<Page<UserEntity>> search(
            @RequestBody UserDTO userParams,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size){
        try {
            return ResponseEntity.ok(userService.getAllUsers(userParams,page,size));
        }catch (Exception exception){
          throw new RuntimeException(exception);
        }
    }
    @GetMapping("get/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable Long id){
        try {
            System.out.println("id " + id);
            Optional<UserEntity> result = userService.getUserById(id);
            return getResponseEntity(result);
        }catch (Exception exception){
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping("update/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable Long id, @RequestBody UserDTO userDetails){
        try {
            Optional<UserEntity> result = userService.updateUser(id,userDetails);
            return getResponseEntity(result);
        }catch (Exception exception){
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping("update-status/{id}")
    public ResponseEntity<Object> updateUserStatus(@PathVariable Long id, @RequestBody UserDTO userDetails){
        try {
            Optional<UserEntity> result = userService.changeStatusUser(id,userDetails.getStatus());
            return getResponseEntity(result);
        }catch (Exception exception){
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping("change-password")
    public ResponseEntity<Object> changePassword( @RequestBody ChangePassDTO changePassReqParams){
        try {
            Optional<UserEntity> result = userService.changePassword(changePassReqParams);
            return getResponseEntity(result);
        }catch (Exception exception){
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping("reset-password")
    public ResponseEntity<Object> resetPassword( @RequestParam String userName){
        System.out.println(userName);
        try {
            Optional<UserEntity> result = userService.resetPassword(userName);
            return getResponseEntity(result);
        }catch (Exception exception){
            return ResponseEntity.badRequest().build();
        }
    }

    private ResponseEntity<Object> getResponseEntity(Optional<UserEntity> result) {
        UserEntity userResponse = new UserEntity();
        userResponse.setId(result.get().getId());
        userResponse.setUsername(result.get().getUsername());
        userResponse.setFirstName(result.get().getFirstName());
        userResponse.setLastName(result.get().getLastName());
        userResponse.setEmail(result.get().getEmail());
        userResponse.setPhoneNumber(result.get().getPhoneNumber());
        userResponse.setAddress(result.get().getAddress());
        userResponse.setRole(result.get().getRole());
        userResponse.setStatus(result.get().getStatus());
        return ResponseEntity.ok(userResponse);
    }

}
