package com.hieutt74.swp391cmsbe.controllers.admin;

import com.hieutt74.swp391cmsbe.dto.request.RoleDTO;
import com.hieutt74.swp391cmsbe.dto.request.UserDTO;
import com.hieutt74.swp391cmsbe.models.Role;
import com.hieutt74.swp391cmsbe.models.UserEntity;
import com.hieutt74.swp391cmsbe.repository.RoleRepository;
import com.hieutt74.swp391cmsbe.service.RoleSevice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/admin/")
@CrossOrigin(origins = "*")
public class RoleController {
    @Autowired
    private RoleSevice roleSevice;


    @PostMapping("role/search")
    public ResponseEntity<Page<Role>> search(
            @RequestBody RoleDTO  roleParams,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size){
        try {
            return ResponseEntity.ok(roleSevice.getAllRoles(roleParams,page,size));
        }catch (Exception exception){
            throw new RuntimeException(exception);
        }
    }
    @PostMapping("role/create")
    public ResponseEntity<Object> createRole(@RequestBody RoleDTO roleParams) {
        try {
            Object createdRole = roleSevice.createRole(roleParams);
            Map<String, Object> responseBody = new HashMap<>();
            responseBody.put("message", "Successfully created");
            responseBody.put("data", createdRole);
            return ResponseEntity.ok(responseBody);
        } catch (Exception exception) {
            Map<String, Object> errorBody = new HashMap<>();
            errorBody.put("message", "Fail to create role");
            errorBody.put("error", exception.getMessage());
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(errorBody);
        }
    }
    @PutMapping("role/update/{id}")
    public ResponseEntity<Object> updateRole(@PathVariable Long id, @RequestBody RoleDTO roleParams) {
        try {
            roleParams.setRoleId(id);
            Object updateRole = roleSevice.updateRole(roleParams);
            Map<String, Object> responseBody = new HashMap<>();
            responseBody.put("message", "Successfully update");
            responseBody.put("data", updateRole);
            return ResponseEntity.ok(responseBody);
        } catch (Exception exception) {
            Map<String, Object> errorBody = new HashMap<>();
            errorBody.put("message", "Fail to update role");
            errorBody.put("error", exception.getMessage());
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(errorBody);
        }
    }

}
