package com.hieutt74.swp391cmsbe.controllers.teacher;

import com.hieutt74.swp391cmsbe.dto.request.ClassCourseDTO;
import com.hieutt74.swp391cmsbe.dto.request.EnrollmentDTO;
import com.hieutt74.swp391cmsbe.dto.response.ClassCourseResponse;
import com.hieutt74.swp391cmsbe.models.ClassCourse;
import com.hieutt74.swp391cmsbe.service.ClassCourseService;
import com.hieutt74.swp391cmsbe.service.EnrollmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/teacher/")
@CrossOrigin(origins = "*")
public class ClassCourseController {
    @Autowired
    private ClassCourseService classCourseService;

    @Autowired
    private EnrollmentService enrollmentService;

    @PostMapping("class-course/all-my-classes")
    public Page<ClassCourseResponse> getAllMyClassCourse(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size,@RequestBody ClassCourseDTO classCourseParams) throws Exception {
        return classCourseService.getAllMyClassCourses(page, size,classCourseParams);
//        return null;
    }

    @PostMapping("class-course/create")
    public ResponseEntity<Optional<ClassCourse>> createClassCourse(@RequestBody ClassCourseDTO classCourseDTO) throws Exception {
        try {
            Optional<ClassCourse> classCourse =  classCourseService.createClassCourse(classCourseDTO);
            return ResponseEntity.ok(classCourse);
        }catch (Exception e){
            throw new Exception(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
        }
    }

    @PutMapping("class-course/update/{id}")
    public ResponseEntity<ClassCourseResponse> updateClassCourse(@RequestParam Long id, @RequestBody ClassCourseDTO classCourseDTO) throws Exception {
//        return courseService.getAllCourses(page, size);
        return null;
    }

    @GetMapping("class-course/get-detail/{id}")
    public ResponseEntity<ClassCourseResponse> getClassCourseById(@PathVariable Long id) throws Exception {
        try {
            ClassCourseResponse classCourse =  classCourseService.getClassCourseById(id);
            return ResponseEntity.ok(classCourse);
        }catch (Exception e){
            throw new Exception(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
        }
    }


//    @GetMapping("class-course/approve-student-enrollments")
//    public void approve(EnrollmentDTO enrollmentDTO) throws Exception {
//        try {
//            enrollmentService.approveEnrollment(enrollmentDTO);
//        }catch (Exception e){
//            throw new Exception(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
//        }
//    }

}
