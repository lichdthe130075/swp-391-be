package com.hieutt74.swp391cmsbe.controllers.teacher;

import com.hieutt74.swp391cmsbe.models.Classes;
import com.hieutt74.swp391cmsbe.models.Semester;
import com.hieutt74.swp391cmsbe.service.ClassesService;
import com.hieutt74.swp391cmsbe.service.SemesterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/teacher/")
@CrossOrigin(origins = "*")
public class ClassTController {
    @Autowired
    private ClassesService classesService;

    @GetMapping("/all-class")
    public ResponseEntity<List<Classes>> getCurrentSemester() {
        List<Classes> listClasses = classesService.getAllClassesTeacher();
        return ResponseEntity.ok(listClasses);
    }
}
