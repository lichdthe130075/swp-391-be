package com.hieutt74.swp391cmsbe.controllers.teacher;

import com.hieutt74.swp391cmsbe.dto.request.CourseDTO;
import com.hieutt74.swp391cmsbe.dto.response.CourseResponse;
import com.hieutt74.swp391cmsbe.models.Course;
import com.hieutt74.swp391cmsbe.service.CourseService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/teacher/")
@CrossOrigin(origins = "*")
public class CourseTController {

    private final CourseService courseService;

    public CourseTController(CourseService courseService) {
        this.courseService = courseService;
    }

    @PostMapping("courses")
    public ResponseEntity<Page<Course>> getAllCourses(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size, @RequestBody CourseDTO courseParams) {
        try {
            Page<Course> courses = courseService.getAllCourses(courseParams, page, size);
            return ResponseEntity.ok(courses);
        } catch (Exception e) {
            System.out.println(e);
            // Log the exception and return an appropriate error response
            return ResponseEntity.status(500).build();
        }
    }

    @GetMapping("courses/{id}")
    public ResponseEntity<CourseResponse> getCourseById(@PathVariable Long id) {
        CourseResponse course = courseService.getCourseById(id);
        if (course != null) {
            return ResponseEntity.ok(course);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("course/create")
    public ResponseEntity<CourseResponse> createCourse(@RequestParam(value = "instructorId") Long instructorId, @RequestBody CourseDTO course) {
        try {
            CourseResponse response = courseService.createCourse(instructorId, course);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            System.out.println(e);
            return ResponseEntity.status(500).build();
        }
    }

    @PutMapping("course/update/{id}")
    public ResponseEntity<CourseResponse> updateCourse( @PathVariable Long id,@RequestParam(value = "instructorId") Long instructorId, @RequestBody CourseDTO course) {
        try {
            CourseResponse updatedCourse = courseService.updateCourse(instructorId, id, course);
            return ResponseEntity.ok(updatedCourse);
        } catch (Exception e) {
            // Log the exception and return an appropriate error response
            return ResponseEntity.status(500).build();
        }
    }

    @DeleteMapping("course/delete/{id}")
    public ResponseEntity<Void> deleteCourse(@PathVariable Long id) {
        try {
            courseService.deleteCourse(id);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            // Log the exception and return an appropriate error response
            return ResponseEntity.status(500).build();
        }
    }

    @DeleteMapping("course/delete-section/{id}")
    public ResponseEntity<Void> deleteSection(@PathVariable Long id) {
        try {
            courseService.deleteSection(id);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            // Log the exception and return an appropriate error response
            return ResponseEntity.status(500).build();
        }
    }
}
