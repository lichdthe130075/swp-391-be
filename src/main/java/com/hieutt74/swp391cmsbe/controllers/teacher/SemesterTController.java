package com.hieutt74.swp391cmsbe.controllers.teacher;

import com.hieutt74.swp391cmsbe.models.Semester;
import com.hieutt74.swp391cmsbe.service.SemesterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api/teacher/")
@CrossOrigin(origins = "*")
public class SemesterTController {
    @Autowired
    private SemesterService semesterService;

    @GetMapping("/current-semester")
    public ResponseEntity<Semester> getCurrentSemester() {
        Optional<Semester> currentSemester = semesterService.getCurrentSemester();

        return currentSemester.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }
}
