package com.hieutt74.swp391cmsbe.controllers.teacher;

import com.hieutt74.swp391cmsbe.models.Classes;
import com.hieutt74.swp391cmsbe.models.UserEntity;
import com.hieutt74.swp391cmsbe.service.ClassesService;
import com.hieutt74.swp391cmsbe.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/teacher/")
@CrossOrigin(origins = "*")
public class ProfileTController {
    @Autowired
    private UserService userService;

    @GetMapping("profile/{id}")
    public ResponseEntity<UserEntity> getProfile(@PathVariable Long id) {
        Optional<UserEntity> userEntity;
        try {
            userEntity = userService.getUserById(id);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return ResponseEntity.ok(userEntity.get());
    }
}
