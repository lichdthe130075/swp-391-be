package com.hieutt74.swp391cmsbe.controllers;

import com.hieutt74.swp391cmsbe.dto.request.LoginDTO;
import com.hieutt74.swp391cmsbe.dto.request.RegisterDTO;
import com.hieutt74.swp391cmsbe.dto.response.AuthResponse;
import com.hieutt74.swp391cmsbe.dto.response.RegisterResponse;
import com.hieutt74.swp391cmsbe.dto.response.UserResponse;
import com.hieutt74.swp391cmsbe.models.Role;
import com.hieutt74.swp391cmsbe.models.UserEntity;
import com.hieutt74.swp391cmsbe.repository.RoleRepository;
import com.hieutt74.swp391cmsbe.repository.AuthRepository;
import com.hieutt74.swp391cmsbe.security.JWTGenerator;
import com.hieutt74.swp391cmsbe.ultils.ExcelService;
import com.hieutt74.swp391cmsbe.ultils.VietnameseToneRemover;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin(origins = "*")
public class AuthenticationController {
    private AuthenticationManager authenticationManager;
    private AuthRepository authRepository;
    private RoleRepository roleRepository;

    private PasswordEncoder passwordEncoder;

    private JWTGenerator jwtGenerator;

    private ExcelService excelService;

    private VietnameseToneRemover vietnameseToneRemover;

    @Autowired
    public AuthenticationController(AuthenticationManager authenticationManager, AuthRepository authRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder, JWTGenerator jwtGenerator,ExcelService excelService,VietnameseToneRemover vietnameseToneRemover) {
        this.authenticationManager = authenticationManager;
        this.authRepository = authRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtGenerator = jwtGenerator;
        this.excelService = excelService;
        this.vietnameseToneRemover = vietnameseToneRemover;
    }



    @PostMapping("register")
    public ResponseEntity<Object> register(@RequestBody RegisterDTO registerDTO){
        try {
            String username = generateUsername(registerDTO.getFirstName(), registerDTO.getLastName());
            if(authRepository.existsByUsername(username)){
                return new ResponseEntity<>("Username had been taken",HttpStatus.BAD_REQUEST);
            }
            if(authRepository.existsByEmail(registerDTO.getEmail())){
                System.out.println("Test email");
                return new ResponseEntity<>("Email had been taken",HttpStatus.BAD_REQUEST);
            }
            Role role = roleRepository.findById(registerDTO.getRole())
                    .orElseThrow(() -> new RuntimeException("Role not found"));

            UserEntity user = new UserEntity();
            user.setUsername(registerDTO.getUsername());
            user.setPassword(passwordEncoder.encode("Password123!"));
            setUserData(registerDTO, role, user);
            System.out.println(authRepository.save(user));
            authRepository.save(user);

            RegisterResponse registerResponse = new RegisterResponse();
            registerResponse.setUsername(user.getUsername());
            registerResponse.setEmail(user.getEmail());

            Map<String, Object> responseBody = new HashMap<>();
            responseBody.put("data",registerResponse);
            responseBody.put("message","User registered successfully");
            responseBody.put("status",HttpStatus.OK);

            return ResponseEntity.ok(responseBody);
        }catch (Exception ex){
            System.out.println(ex.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }



    @PostMapping("upload-users")
    public ResponseEntity<Map<String, Object>> uploadUsers(@RequestParam("file") MultipartFile file) {
        Map<String, Object> responseBody = new HashMap<>();
        List<String> errors = new ArrayList<>();

        try {
            List<RegisterDTO> users = excelService.parseExcelFile(file);
            for (RegisterDTO registerDTO : users) {
                try {
                    String username = generateUsername(registerDTO.getFirstName(), registerDTO.getLastName());

                    if (authRepository.existsByUsername(username)) {
                        errors.add("Username " + username + " already exists for email: " + registerDTO.getEmail());
                        continue; // Skip existing usernames
                    }
                    if (authRepository.existsByEmail(registerDTO.getEmail())) {
                        errors.add("Email " + registerDTO.getEmail() + " already exists.");
                        continue; // Skip existing emails
                    }

                    Role role = roleRepository.findById(registerDTO.getRole())
                            .orElseThrow(() -> new RuntimeException("Role not found: " + registerDTO.getRole()));

                    UserEntity user = new UserEntity();
                    user.setUsername(username);
                    user.setPassword(passwordEncoder.encode("Password123!"));
                    setUserData(registerDTO, role, user);
                    authRepository.save(user);

                } catch (Exception e) {
                    errors.add("Error processing user with email: " + registerDTO.getEmail() + " - " + e.getMessage());
                }
            }

            responseBody.put("message", "Users uploaded and registered successfully");
            responseBody.put("status", HttpStatus.OK);
            responseBody.put("errors", errors);

            return ResponseEntity.ok(responseBody);
        } catch (Exception ex) {
            ex.printStackTrace();
            responseBody.put("message", "An error occurred while processing the file");
            responseBody.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
            responseBody.put("errors", Collections.singletonList(ex.getMessage()));
            return new ResponseEntity<>(responseBody, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    private void setUserData(RegisterDTO registerDTO, Role role, UserEntity user) {
        user.setFirstName(vietnameseToneRemover.removeVietnameseTones(registerDTO.getFirstName()));
        user.setLastName(vietnameseToneRemover.removeVietnameseTones(registerDTO.getLastName()));
        user.setAddress(vietnameseToneRemover.removeVietnameseTones(registerDTO.getAddress()));
        user.setPhoneNumber(registerDTO.getPhoneNumber());
        user.setRole(role);
        user.setStatus(1);
        user.setEmail(registerDTO.getEmail());
    }

    @PostMapping("login")
    public ResponseEntity<Object> login(@RequestBody LoginDTO loginDTO){
        if(authRepository.existsByUsername(loginDTO.getUsername())){
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getUsername(),loginDTO.getPassword()));
            System.out.println(authentication);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = jwtGenerator.tokenGenerate(authentication);

            Optional<UserEntity> user = authRepository.findByUsername(loginDTO.getUsername());

            UserResponse userResponse = new UserResponse();
            userResponse.setId(user.get().getId());
            userResponse.setUsername(user.get().getUsername());
            userResponse.setFirstName(user.get().getFirstName());
            userResponse.setLastName(user.get().getLastName());
            userResponse.setEmail(user.get().getEmail());
            userResponse.setAddress(user.get().getAddress());
            userResponse.setPhoneNumber(user.get().getPhoneNumber());
            userResponse.setRole(user.get().getRole());

            Map<String, Object> responseBody = new HashMap<>();
            responseBody.put("userData",userResponse);
            responseBody.put("accessToken",new AuthResponse(token));
            responseBody.put("message","User fetched successfully");
            responseBody.put("status",HttpStatus.OK);

            return ResponseEntity.ok(responseBody);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

    }


    private String generateUsername(String firstName, String lastName) {
        String baseUsername = (firstName + " " + lastName).toLowerCase();
        String removeToneStr = vietnameseToneRemover.removeVietnameseTones(baseUsername);
        String[] nameParts = removeToneStr.trim().split(" ");
        StringBuilder username = new StringBuilder(nameParts[nameParts.length - 1]);
        for (int i = 0; i < nameParts.length - 1; i++) {
            String[] namePartsElmTemp = nameParts[i].trim().split("");
            username.append(namePartsElmTemp[0]);
        }
        int counter = authRepository.countByUsernameContainsIgnoreCase(username.toString());
        do {
            counter++;
            username.append(counter);
        }while (authRepository.existsByUsername(username.toString()));
        return username.toString();
    }




}
