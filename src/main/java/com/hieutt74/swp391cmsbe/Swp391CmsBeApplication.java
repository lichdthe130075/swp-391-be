package com.hieutt74.swp391cmsbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
public class Swp391CmsBeApplication {
	public static void main(String[] args) {
		SpringApplication.run(Swp391CmsBeApplication.class, args);
	}
}
