package com.hieutt74.swp391cmsbe.repository;

import com.hieutt74.swp391cmsbe.dto.response.ClassCourseResponse;
import com.hieutt74.swp391cmsbe.models.ClassCourse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClassCourseRepository extends JpaRepository<ClassCourse,Long>, JpaSpecificationExecutor<ClassCourse> {
    Optional<ClassCourse> findById(Long id);
//    Page<ClassCourseResponse> findAllMyClassCourses(Specification<ClassCourse> spec, Pageable pageable);

}
