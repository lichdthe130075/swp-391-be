package com.hieutt74.swp391cmsbe.repository;

import com.hieutt74.swp391cmsbe.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface RoleRepository extends JpaRepository<Role,Integer>, JpaSpecificationExecutor<Role> {

    Optional<Role> findById(Long id);
    Optional<Role> findByRoleName(String name);
}
