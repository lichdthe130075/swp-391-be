package com.hieutt74.swp391cmsbe.repository;

import com.hieutt74.swp391cmsbe.models.Major;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MajorRepository extends JpaRepository<Major, Long> {
}
