package com.hieutt74.swp391cmsbe.repository;

import com.hieutt74.swp391cmsbe.models.Enrollment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface EnrollmentRepository extends JpaRepository<Enrollment, Long>, JpaSpecificationExecutor<Enrollment> {

   List<Enrollment> findByStudent_Id(Long studentId);

    @Query("SELECT e FROM Enrollment e WHERE e.classCourse.id = :classCourseId")
    List<Enrollment> findByClassCourseId(@Param("classCourseId") Long classCourseId);

    @Query("SELECT e FROM Enrollment e WHERE e.student.id = :studentId")
    Page<Enrollment> findByStudentId(@Param("studentId") Long studentId, Pageable pageable);
}
