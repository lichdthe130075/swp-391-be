package com.hieutt74.swp391cmsbe.repository;

import com.hieutt74.swp391cmsbe.models.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long>, JpaSpecificationExecutor<Course> {
      Page<Course> findAll(Specification<Course> spec, Pageable pageable);

      void deleteById(Long id);

      Boolean existsByCourseCode(String courseCode);
}
