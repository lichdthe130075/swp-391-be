package com.hieutt74.swp391cmsbe.repository;

import com.hieutt74.swp391cmsbe.models.UserEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.Optional;

@Repository
public interface AuthRepository extends JpaRepository<UserEntity,Integer> {
    Optional<UserEntity> findByUsername(String username);
    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    Integer countByUsernameContainsIgnoreCase(String username);


}
