package com.hieutt74.swp391cmsbe.repository;

import com.hieutt74.swp391cmsbe.models.Section;
import com.hieutt74.swp391cmsbe.models.Semester;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;
@Repository
public interface SemesterRepository extends JpaRepository<Semester,Long>, JpaSpecificationExecutor<Semester> {

    @Query("SELECT s FROM Semester s WHERE :currentDate BETWEEN s.startDate AND s.endDate")
    Optional<Semester> findCurrentSemester(LocalDate currentDate);
}
