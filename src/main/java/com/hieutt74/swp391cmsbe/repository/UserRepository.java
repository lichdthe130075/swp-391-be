package com.hieutt74.swp391cmsbe.repository;

import com.hieutt74.swp391cmsbe.models.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface UserRepository extends JpaRepository<UserEntity,Long>, JpaSpecificationExecutor<UserEntity> {
    Page<UserEntity> findAll(Pageable pageable);

    Optional<UserEntity> findById(Long id);

    Optional<UserEntity> findByUsername(String userName);

    Optional<UserEntity> findByEmail(String email);

}
