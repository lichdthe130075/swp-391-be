package com.hieutt74.swp391cmsbe.repository;

import com.hieutt74.swp391cmsbe.dto.response.SectionResponse;
import com.hieutt74.swp391cmsbe.models.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;
@Repository
public interface SectionRepository extends JpaRepository<Section,Long> {

    Set<SectionResponse> findByCourse_Id(Long courseId);



//    @Query("DELETE from Course c where c.id = :id")
    void deleteByCourse_Id(Long id);
}
