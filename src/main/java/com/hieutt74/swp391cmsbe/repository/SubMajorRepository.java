package com.hieutt74.swp391cmsbe.repository;

import com.hieutt74.swp391cmsbe.models.SubMajor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubMajorRepository extends JpaRepository<SubMajor, Long> {
}
