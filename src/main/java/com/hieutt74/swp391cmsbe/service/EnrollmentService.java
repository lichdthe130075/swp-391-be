package com.hieutt74.swp391cmsbe.service;

import com.hieutt74.swp391cmsbe.dto.request.EnrollmentDTO;
import com.hieutt74.swp391cmsbe.dto.response.ClassCourseResponse;
import com.hieutt74.swp391cmsbe.dto.response.CourseResponse;
import com.hieutt74.swp391cmsbe.dto.response.EnrollmentResponse;
import com.hieutt74.swp391cmsbe.dto.response.UserResponse;
import com.hieutt74.swp391cmsbe.models.*;
import com.hieutt74.swp391cmsbe.repository.*;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class EnrollmentService {
    @Autowired
    private EnrollmentRepository enrollmentRepository;

    @Autowired
    private ClassCourseRepository classCourseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ClassesRepository classesRepository;

    @Autowired
    private SemesterRepository semesterRepository;

    @Autowired
    private CourseService courseService;

    @Transactional
    public Enrollment createEnrollment(EnrollmentDTO enrollmentDTO) throws Exception {
        if (enrollmentDTO == null) {
            throw new IllegalArgumentException("EnrollmentDTO cannot be null");
        }
        try {
            UserEntity student = userRepository.findById(enrollmentDTO.getStudentId())
                    .orElseThrow(() -> new RuntimeException("Student's not found"));
            System.out.println(student);
            ClassCourse classCourse = classCourseRepository.findById(enrollmentDTO.getClassCourseId())
                    .orElseThrow(() -> new RuntimeException("Class Course's not found"));
            System.out.println(classCourse);
            Enrollment enrollment = new Enrollment();
            enrollment.setStudent(student);
            enrollment.setClassCourse(classCourse);
            enrollment.setEnrollmentStatus(0);
            enrollment.setEnrollmentDate(LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault()));

            return enrollmentRepository.save(enrollment);
        } catch (EntityNotFoundException ex) {
            throw new Exception("User or ClassCourse not found", ex);
        } catch (Exception ex) {
            throw new Exception("Failed to create enrollment", ex);
        }
    }

    @Transactional(readOnly = true)
    public List<EnrollmentResponse> getAllEnrollmentByStudentId(Long studentID) throws Exception {
        try {
            List<Enrollment> enrollments = enrollmentRepository.findByStudent_Id(studentID);
            List<EnrollmentResponse> listRes = new ArrayList<>();
            if (!enrollments.isEmpty()) {
                for (Enrollment enrollment : enrollments) {
                    EnrollmentResponse enrollmentResponse = getDetailEnrolledClass(enrollment.getId());
                    listRes.add(enrollmentResponse);
                }
                return listRes;
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    @Transactional(readOnly = true)
    public Page<EnrollmentResponse> getAllEnrollmentByStudentId(Long studentId, Pageable pageable) throws Exception {
        try {
            Page<Enrollment> enrollments = enrollmentRepository.findByStudentId(studentId, pageable);
            List<EnrollmentResponse> enrollmentResponses = new ArrayList<>();

            for (Enrollment enrollment : enrollments) {
                EnrollmentResponse enrollmentResponse = getDetailEnrolledClass(enrollment.getId());
                enrollmentResponses.add(enrollmentResponse);
            }

            return new PageImpl<>(enrollmentResponses, pageable, enrollments.getTotalElements());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    @Transactional(readOnly = true)
    public EnrollmentResponse getDetailEnrolledClass(Long enrollmentId) throws Exception {
        try {
            Optional<Enrollment> enrollment = enrollmentRepository.findById(enrollmentId);
            EnrollmentResponse enrollmentResponse = new EnrollmentResponse();
            if (enrollment.isPresent()) {
                ClassCourseResponse classCourseResponse = getClassCourseById(enrollment.get().getClassCourse().getId());
                UserEntity student = userRepository.findById(enrollment.get().getStudent().getId())
                        .orElseThrow(() -> new RuntimeException("Student's not found"));
                UserResponse studentResponse = new UserResponse();

                studentResponse.setId(student.getId());
                studentResponse.setFirstName(student.getFirstName());
                studentResponse.setLastName(student.getLastName());

                enrollmentResponse.setId(enrollment.get().getId());
                enrollmentResponse.setClassCourse(classCourseResponse);
                enrollmentResponse.setStudent(studentResponse);
                enrollmentResponse.setEnrollmentDate(enrollment.get().getEnrollmentDate());
                enrollmentResponse.setEnrollmentStatus(enrollment.get().getEnrollmentStatus());
            }
            return enrollmentResponse;
        } catch (Exception ex) {
            return null;
        }
    }

    private ClassCourseResponse getClassCourseById(Long id) {
        try {
            ClassCourseResponse classCourseResponse = new ClassCourseResponse();
            ClassCourse classCourse = classCourseRepository.getReferenceById(id);
            if (classCourse != null) {
                Classes classObj = classesRepository.findById(classCourse.getClasses().getId())
                        .orElseThrow(() -> new RuntimeException("Class's not found"));
                Semester semester = semesterRepository.findById(classCourse.getSemester().getId())
                        .orElseThrow(() -> new RuntimeException("Semester's not found"));
                CourseResponse course = courseService.getCourseById(classCourse.getCourse().getId());
                UserEntity instructor = userRepository.findById(classCourse.getInstructor().getId())
                        .orElseThrow(() -> new RuntimeException("Instructor's not found"));
                classCourseResponse.setClasses(classObj);
                classCourseResponse.setSemester(semester);
                classCourseResponse.setCourse(course);
                classCourseResponse.setInstructor(instructor);
                classCourseResponse.setId(classCourse.getId());
            }
            return classCourseResponse;
        } catch (Exception ex) {
            return null;
        }

    }

    // @Transactional
    // public void approveEnrollment (EnrollmentDTO enrollmentDTO) throws Exception
    // {
    // try {
    // Enrollment enrollment =
    // enrollmentRepository.findById(enrollmentDTO.getEnrollmentId()).orElse(null);
    // if (enrollment != null) {
    // enrollment.setEnrollmentStatus(1);
    // enrollmentRepository.save(enrollment);
    // }
    // }catch (Exception ex){
    // throw new Exception(ex.getMessage());
    // }
    // }
}
