package com.hieutt74.swp391cmsbe.service;


import com.hieutt74.swp391cmsbe.dto.request.ChangePassDTO;
import com.hieutt74.swp391cmsbe.dto.request.UserDTO;
import com.hieutt74.swp391cmsbe.dto.specification.UserSpecification;
import com.hieutt74.swp391cmsbe.models.Role;
import com.hieutt74.swp391cmsbe.models.UserEntity;
import com.hieutt74.swp391cmsbe.repository.RoleRepository;
import com.hieutt74.swp391cmsbe.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserSpecification userSpecification;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional(readOnly = true)
    public Page<UserEntity> getAllUsers(UserDTO userParam, int page, int size) throws Exception {
        Pageable pageable = PageRequest.of(page, size);
        Specification<UserEntity> spec = userSpecification.createSpecification(userParam);
        Page<UserEntity> pageResult = userRepository.findAll(spec, pageable); // Retrieve a page of data with specification
        return pageResult.map(userResponse -> modelMapper.map(userResponse, UserEntity.class));
    }


    @Transactional(readOnly = true)
    public Optional<UserEntity> getUserById(Long id) throws Exception{
        Optional<UserEntity> userResponse = Optional.ofNullable(userRepository.findById(id).orElseThrow(() -> new RuntimeException("Course not found")));
        if (userResponse.isPresent()) {
            Optional<Role> role = roleRepository.findById(userResponse.get().getRole().getId());
            userResponse.get().setRole(role.get());
            return userResponse;
        }
        return null;
    }

    @Transactional
    public Optional<UserEntity> updateUser (Long id, UserDTO userDetails) throws Exception{
        Optional<UserEntity> user = getUserById(id);
        if(user.isPresent()){
            user.get().setUsername(userDetails.getUsername());
            user.get().setFirstName(userDetails.getFirstName());
            user.get().setLastName(userDetails.getLastName());
            user.get().setAddress(userDetails.getAddress());
            user.get().setPhoneNumber(userDetails.getPhoneNumber());
            userRepository.save(user.get());
        }
        return getUserById(id);

//        return null;
    }

    @Transactional
    public Optional<UserEntity> changeStatusUser (Long id, Integer userStatus) throws Exception{
        Optional<UserEntity> user = getUserById(id);
        if(user.isPresent()){
            user.get().setStatus(userStatus);
            userRepository.save(user.get());
            return getUserById(id);
        }
        return null;
    }

    @Transactional
    public Optional<UserEntity> changePassword (ChangePassDTO changePassReqParams) throws Exception{

        Optional<UserEntity> user = userRepository.findByUsername(changePassReqParams.getUsername());
        if(user.isPresent()){
            if(passwordEncoder.matches(changePassReqParams.getOldPassword(), user.get().getPassword())) {
                user.get().setPassword(passwordEncoder.encode(changePassReqParams.getNewPassword()));
                userRepository.save(user.get());
                return  userRepository.findByUsername(changePassReqParams.getUsername());
            }
        }
        return null;
    }

    @Transactional
    public Optional<UserEntity> resetPassword (String userName) throws Exception{
        System.out.println(userName);
        Optional<UserEntity> user = userRepository.findByUsername(userName);
        System.out.println(user);
        if(user.isPresent()){
            user.get().setPassword(passwordEncoder.encode("password@123!"));
            userRepository.save(user.get());
            return  userRepository.findByUsername(userName);
        }
        return null;
    }
}
