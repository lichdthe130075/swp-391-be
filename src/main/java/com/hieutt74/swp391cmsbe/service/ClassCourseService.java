package com.hieutt74.swp391cmsbe.service;

import com.hieutt74.swp391cmsbe.dto.request.ClassCourseDTO;
import com.hieutt74.swp391cmsbe.dto.response.ClassCourseResponse;
import com.hieutt74.swp391cmsbe.dto.response.CourseResponse;
import com.hieutt74.swp391cmsbe.dto.specification.ClassCourseSpecification;
import com.hieutt74.swp391cmsbe.models.*;
import com.hieutt74.swp391cmsbe.repository.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ClassCourseService {


    @Autowired
    private ClassCourseRepository classCourseRepository;

    @Autowired
    private ClassesRepository classesRepository;

    @Autowired
    private SemesterRepository semesterRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CourseService courseService;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private ClassCourseSpecification classCourseSpecification;

    @Autowired
    private EnrollmentRepository enrollmentRepository;

    @Transactional(readOnly = true)
    public Page<ClassCourseResponse> getAllMyClassCourses(int page, int size, ClassCourseDTO classCourseParams) {
        System.out.println(classCourseParams);
        Pageable pageable = PageRequest.of(page, size);
        Specification<ClassCourse> specification = classCourseSpecification.createSpecification(classCourseParams);
        Page<ClassCourse> classCourses = classCourseRepository.findAll(specification, pageable);
        return classCourses.map(course -> modelMapper.map(course, ClassCourseResponse.class));
    }
    public Optional<ClassCourse> createClassCourse(ClassCourseDTO classCourseDTO) {
        try {
            ClassCourse classCourse = new ClassCourse();
            Classes classObj = classesRepository.findById(classCourseDTO.getClassId()).orElseThrow(() -> new RuntimeException("Class's not found"));
            Semester semester = semesterRepository.findById(classCourseDTO.getSemesterId()).orElseThrow(() -> new RuntimeException("Semester's not found"));
            Course course = courseRepository.findById(classCourseDTO.getCourseId()).orElseThrow(() -> new RuntimeException("Course's not found"));
            UserEntity instructor = userRepository.findById(classCourseDTO.getInstructorId()).orElseThrow(() -> new RuntimeException("Instructor's not found"));

            classCourse.setClasses(classObj);
            classCourse.setSemester(semester);
            classCourse.setCourse(course);
            classCourse.setInstructor(instructor);
            classCourse.setCreatedBy(classCourseDTO.getCreatedBy());
            classCourse.setCreatedDate(LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault()));
            ClassCourse currentClassCourse =  classCourseRepository.save(classCourse);

            return classCourseRepository.findById(currentClassCourse.getId());
        }catch (Exception ex){
            return Optional.empty();
        }

    }


    public ClassCourseResponse getClassCourseById( Long id) {
        try {
            ClassCourseResponse classCourseResponse = new ClassCourseResponse();
            ClassCourse classCourse = classCourseRepository.getReferenceById(id);
            Classes classObj = classesRepository.findById(classCourse.getClasses().getId()).orElseThrow(() -> new RuntimeException("Class's not found"));
            Semester semester = semesterRepository.findById(classCourse.getSemester().getId()).orElseThrow(() -> new RuntimeException("Semester's not found"));
            CourseResponse course = courseService.getCourseById(classCourse.getCourse().getId());
            UserEntity instructor = userRepository.findById(classCourse.getInstructor().getId()).orElseThrow(() -> new RuntimeException("Instructor's not found"));
            List<Enrollment> enrolledStudents = enrollmentRepository.findByClassCourseId(id);
            classCourseResponse.setId(classCourse.getId());
            classCourseResponse.setClasses(classObj);
            classCourseResponse.setSemester(semester);
            classCourseResponse.setCourse(course);
            classCourseResponse.setInstructor(instructor);
            classCourseResponse.setEnrolledStudents(enrolledStudents);
            return classCourseResponse;
        }catch (Exception ex){
            return null;
        }

    }


//    @Transactional(readOnly = true)
//    public List<ClassCourseResponse> studentSearchClass(ClassCourseDTO searchParams) {
//        Specification<ClassCourse> specification = classCourseSpecification.createSpecification(searchParams);
//        List<ClassCourse> tempList = classCourseRepository.findAll(specification);
//        List<ClassCourseResponse> responseList = new ArrayList<>();
//
//        for (ClassCourse classCourse : tempList) {
//            ClassCourseResponse classCourseResponse =getClassCourseById(classCourse.getId());
//            responseList.add(classCourseResponse);
//        }
//
//        return responseList;
//    }


    @Transactional(readOnly = true)
    public Page<ClassCourseResponse> studentSearchClass(int page, int size, ClassCourseDTO classCourseParams) {
        System.out.println(classCourseParams);
        Pageable pageable = PageRequest.of(page, size);
        Specification<ClassCourse> specification = classCourseSpecification.createSpecification(classCourseParams);
        Page<ClassCourse> classCourses = classCourseRepository.findAll(specification, pageable);
        return classCourses.map(course -> modelMapper.map(course, ClassCourseResponse.class));
    }


    public ClassCourseResponse getClassCourseStudentById( Long id) {
        try {
            ClassCourseResponse classCourseResponse = new ClassCourseResponse();
            ClassCourse classCourse = classCourseRepository.getReferenceById(id);
            Classes classObj = classesRepository.findById(classCourse.getClasses().getId()).orElseThrow(() -> new RuntimeException("Class's not found"));
            Semester semester = semesterRepository.findById(classCourse.getSemester().getId()).orElseThrow(() -> new RuntimeException("Semester's not found"));
            CourseResponse course = courseService.getCourseById(classCourse.getCourse().getId());
            UserEntity instructor = userRepository.findById(classCourse.getInstructor().getId()).orElseThrow(() -> new RuntimeException("Instructor's not found"));

            classCourseResponse.setId(classCourse.getId());
            classCourseResponse.setClasses(classObj);
            classCourseResponse.setSemester(semester);
            classCourseResponse.setCourse(course);
            classCourseResponse.setInstructor(instructor);
            return classCourseResponse;
        }catch (Exception ex){
            return null;
        }

    }



}
