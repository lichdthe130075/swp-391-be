package com.hieutt74.swp391cmsbe.service;

import com.hieutt74.swp391cmsbe.dto.request.RoleDTO;
import com.hieutt74.swp391cmsbe.dto.request.UserDTO;
import com.hieutt74.swp391cmsbe.dto.specification.RoleSpecification;
import com.hieutt74.swp391cmsbe.exception.ApiException;
import com.hieutt74.swp391cmsbe.models.Role;
import com.hieutt74.swp391cmsbe.models.UserEntity;
import com.hieutt74.swp391cmsbe.repository.RoleRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
@Service
public class RoleSevice {

    @Autowired
    private RoleRepository roleRepository;
    DateTimeFormatter dtf = DateTimeFormatter
            .ofPattern("[MM/dd/yyyy HH:mm:ss a][M/dd/yyyy h:m:s a][MM/d/yyyy HH:mm:s a]");
    @Autowired
private RoleSpecification roleSpecification;
    @Autowired
    private ModelMapper modelMapper;

    @Transactional(readOnly = true)
    public Page<Role> getAllRoles(RoleDTO roleParam, int page, int size) throws Exception {
        Pageable pageable = PageRequest.of(page, size);
        Specification<Role> spec = roleSpecification.createSpecification(roleParam);
        Page<Role> pageResult = roleRepository.findAll(spec, pageable); // Retrieve a page of data with specification
        return pageResult.map(roleResponse -> modelMapper.map(roleResponse, Role.class));
    }


    @Transactional
    public Role createRole (RoleDTO roleDTO) throws Exception{
        Role role = null;
       try {
           if(roleDTO != null){
               role = new Role();
               role.setRoleDescription(roleDTO.getRoleDescription());
               role.setRoleName(roleDTO.getRoleName());
               role.setCreatedBy(roleDTO.getCreateBy());
               role.setLastModifiedBy(null);
               role.setCreatedDate(LocalDateTime.now());

               roleRepository.save(role);
           }
       }catch (Exception ex){
           return null;

       }
        return role;

//        return null;
    }

//    @Transactional
//    public Role updateRole (RoleDTO roleDTO) throws Exception{
//        Role role = null;
//        try {
//            if(roleDTO != null){
//                Optional<Role> roleOptional = roleRepository.findByRoleName(roleDTO.getRoleName());
//                if (roleOptional.isPresent()) {
//
//
//                   throw ApiException.badRequestException("Duplicate role name");
//                }
//                role = new Role();
//                role.setRoleDescription(roleDTO.getRoleDescription());
//                role.setRoleName(roleDTO.getRoleName());
//                role.setCreatedBy(roleDTO.getCreateBy());
//                role.setLastModifiedBy(LocalDateTime.parse(roleDTO.getUpdateOn(),dtf));
//                role.setId(roleDTO.getRoleId());
//                roleRepository.save(role);
//            }
//        }catch (Exception ex){
//            return null;
//
//        }
//        return role;
//
////        return null;
//    }

    @Transactional
    public Role updateRole (RoleDTO roleDTO) throws Exception{
       Optional<Role> roleOptional = roleRepository.findById(roleDTO.getRoleId());
       if(roleOptional.isEmpty()){
           throw ApiException.badRequestException("Not found role");
       }
       Role role = roleOptional.get();
       role.setRoleDescription(roleDTO.getRoleDescription());
        role.setRoleName(roleDTO.getRoleName());
        role.setLastModifiedBy(roleDTO.getUpdateBy());
        role.setLastModifiedDate(LocalDateTime.now());
       return roleRepository.save(role);
    }
}
