package com.hieutt74.swp391cmsbe.service;

import com.hieutt74.swp391cmsbe.dto.request.ClassesDTO;
import com.hieutt74.swp391cmsbe.dto.request.CourseDTO;
import com.hieutt74.swp391cmsbe.dto.response.ClassResponse;
import com.hieutt74.swp391cmsbe.dto.specification.ClassSpecification;
import com.hieutt74.swp391cmsbe.models.*;
import com.hieutt74.swp391cmsbe.repository.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ClassesService {
    @Autowired
    private ClassesRepository classRepository;

    @Autowired
    private MajorRepository majorRepository;

    @Autowired
    private SubMajorRepository subMajorRepository;


    private ClassSpecification classSpecification;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private SemesterRepository semesterRepository;

    // get all classes method
    @Transactional(readOnly = true)
    public Page<Classes> getAllClasses(ClassesDTO classParams, int page, int size) {
        Pageable pageable = PageRequest.of(page,size);
        Specification<Classes> specification = classSpecification.createSpecification(classParams);
        Page<Classes> pageResult = classRepository.findAll(specification,pageable); // Retrieve a page of data
        return pageResult.map(classes -> modelMapper.map(classes, Classes.class));
    }

    @Transactional(readOnly = true)
    public List<Classes> getAllClassesTeacher() {
        return classRepository.findAll();
    }

    // create method
    @Transactional
    public Classes createClass(ClassesDTO classesDTO) {
        Classes classes = new Classes();
        classes.setClassCode(classesDTO.getClassCode());

        Major major = majorRepository.findById(classesDTO.getMajorId()).orElse(null);
        if (major == null) {
            throw new RuntimeException("Major not found");
        }
        classes.setMajor(major);

        if (classesDTO.getSubMajorId() != null) {
            SubMajor subMajor = subMajorRepository.findById(classesDTO.getSubMajorId()).orElse(null);
            if (subMajor == null) {
                throw new RuntimeException("Sub major not found");
            }else {
                classes.setSubMajor(subMajor);
            }
        }
        classes.setCreatedBy(classesDTO.getCreatedBy());
        classes.setCreatedDate( LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault()));

        return classRepository.save(classes);
    }

    // get class by id method
    @Transactional(readOnly = true)
    public ClassResponse getClassById(Long id) {
        Optional<Classes> classes = classRepository.findById(id);
        if (classes.isPresent()) {

            ClassResponse classResponse = new ClassResponse();
            classResponse.setId(id);
            classResponse.setClassCode(classes.get().getClassCode());
            classResponse.setMajor(classes.get().getMajor());
            classResponse.setSubMajor(classes.get().getSubMajor());
            classResponse.setCreatedDate(classes.get().getCreatedDate());

            if (classes.get().getCreatedBy() != null) {
                Optional<UserEntity> createdBy = userRepository.findById(classes.get().getCreatedBy());
                classResponse.setCreatedBy(createdBy.get());
            }

            classResponse.setCreatedDate(classes.get().getCreatedDate());

            if (classes.get().getLastModifiedBy() != null) {
                Optional<UserEntity> lastModifiedBy = userRepository.findById(classes.get().getLastModifiedBy());
                classResponse.setLastModifiedBy(lastModifiedBy.get());
            }

            classResponse.setLastModifiedDate(classes.get().getLastModifiedDate());

            return classResponse;
        }
        return null;
    }

    // update class method
    @Transactional
    public Optional<ClassesDTO> updateClass(ClassesDTO classesDTO) {

        Optional<Classes> classes = classRepository.findById(classesDTO.getId());
        if (classes.isPresent()) {

            Long majorId = classesDTO.getMajorId();
            Major major = majorRepository.findById(majorId).orElse(null);
            if (major == null) {
                throw new RuntimeException("Major not found");
            }

            classes.get().setClassCode(classesDTO.getClassCode());
            classes.get().setMajor(major);

            Long subMajorId = classesDTO.getSubMajorId();
            if (subMajorId != null) {
                SubMajor subMajor = subMajorRepository.findById(subMajorId).orElse(null);
                if (subMajor == null) {
                    throw new RuntimeException("Sub major not found");
                }else {
                    classes.get().setSubMajor(subMajor);
                }
            }
            classes.get().setLastModifiedBy(classesDTO.getCreatedBy());
            classes.get().setLastModifiedDate( LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault()));

            classRepository.save(classes.get());
            return Optional.of(classesDTO);
        }
        return Optional.empty();
    }

    // delete class method
    public void deleteClass(Long id) {
        classRepository.deleteById(id);
    }
}
