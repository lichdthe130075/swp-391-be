package com.hieutt74.swp391cmsbe.service;

import com.hieutt74.swp391cmsbe.dto.request.MajorDTO;
import com.hieutt74.swp391cmsbe.dto.request.SectionDTO;
import com.hieutt74.swp391cmsbe.dto.request.SubMajorDTO;
import com.hieutt74.swp391cmsbe.models.Major;
import com.hieutt74.swp391cmsbe.models.Section;
import com.hieutt74.swp391cmsbe.models.SubMajor;
import com.hieutt74.swp391cmsbe.repository.MajorRepository;
import com.hieutt74.swp391cmsbe.repository.SubMajorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MajorService {
    @Autowired
    private MajorRepository majorRepository;
     @Autowired
     private SubMajorRepository subMajorRepository;
    private SubMajorService subMajorService;

    // create method
    public Major createMajor(MajorDTO majorDTO) {
        try {
            Major major = new Major();
            major.setName(majorDTO.getName());
            major.setAcronym(majorDTO.getAcronym());
            major.setDescription(majorDTO.getDescription());
            major.setCreatedBy(majorDTO.getManagerId());
            major.setCreatedDate( LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault()));
            Major majorRes =  majorRepository.save(major);
            if(!majorDTO.getSubMajors().isEmpty()){
                for (SubMajorDTO subMajorDTO : majorDTO.getSubMajors()) {
                    SubMajor subMajor = new SubMajor();
                    subMajor.setMajor(majorRes);
                    subMajor.setName(subMajorDTO.getName());
                    subMajor.setAcronym(subMajorDTO.getAcronym());
                    subMajor.setField(subMajorDTO.getField());
                    subMajor.setCreatedBy(subMajorDTO.getManagerId());
                    subMajor.setCreatedDate( LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault()));
                    subMajorRepository.save(subMajor);
                }
            }
            return  majorRepository.getReferenceById(majorRes.getId());
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    // get all majors
//    public List<MajorDTO> getAllMajors() {
//        List<Major> majors = majorRepository.findAll();
//        List<MajorDTO> majorDTOs = new ArrayList<>();
//        for (Major major : majors) {
//            MajorDTO majorDTO = new MajorDTO();
//            majorDTO.setId(major.getId());
//            majorDTO.setName(major.getName());
//            majorDTO.setDescription(major.getDescription());
//            majorDTOs.add(majorDTO);
//        }
//        return majorDTOs;
//    }

    // get major by id
//    public MajorDTO getMajorById(Long id) {
//        Major major = majorRepository.findById(id).orElse(null);
//        if (major != null) {
//            MajorDTO majorDTO = new MajorDTO();
//            majorDTO.setId(major.getId());
//            majorDTO.setName(major.getName());
//            majorDTO.setDescription(major.getDescription());
//            return majorDTO;
//        }
//        return null;
//    }

    // update major
    public MajorDTO updateMajor(Long id, MajorDTO majorDTO) {
        Major major = majorRepository.findById(id).orElse(null);
        if (major != null) {
            major.setName(majorDTO.getName());
            major.setDescription(majorDTO.getDescription());
            majorRepository.save(major);
            return majorDTO;
        }
        return null;
    }

    // delete major
    public void deleteMajor(Long id) {
        majorRepository.deleteById(id);
    }
}
