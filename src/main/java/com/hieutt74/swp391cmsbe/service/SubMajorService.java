package com.hieutt74.swp391cmsbe.service;

import com.hieutt74.swp391cmsbe.dto.request.SubMajorDTO;
import com.hieutt74.swp391cmsbe.models.Major;
import com.hieutt74.swp391cmsbe.models.SubMajor;
import com.hieutt74.swp391cmsbe.repository.MajorRepository;
import com.hieutt74.swp391cmsbe.repository.SubMajorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SubMajorService {
    @Autowired
    private SubMajorRepository subMajorRepository;

    @Autowired
    private MajorRepository majorRepository;

    // create method
    public SubMajor createSubMajor(Long majorId,SubMajorDTO subMajorDTO) {
        Major major = majorRepository.findById(majorId).orElse(null);
        if (major == null) {
            throw new RuntimeException("Major not found");
        }
        SubMajor subMajor = new SubMajor();
        subMajor.setName(subMajorDTO.getName());
        subMajor.setField(subMajorDTO.getField());
        subMajor.setMajor(major);
        return subMajorRepository.save(subMajor);
    }

    // get all sub majors
//    public List<SubMajorDTO> getAllSubMajors() {
//        List<SubMajor> subMajors = subMajorRepository.findAll();
//        List<SubMajorDTO> subMajorDTOs = new ArrayList<>();
//        for (SubMajor subMajor : subMajors) {
//            SubMajorDTO subMajorDTO = new SubMajorDTO();
//            subMajorDTO.setId(subMajor.getId());
//            subMajorDTO.setName(subMajor.getName());
//            subMajorDTO.setField(subMajor.getField());
//            subMajorDTO.setMajorId(subMajor.getMajor().getId());
//            subMajorDTOs.add(subMajorDTO);
//        }
//        return subMajorDTOs;
//    }

    // get sub major by id
//    public SubMajorDTO getSubMajorById(Long id) {
//        SubMajor subMajor = subMajorRepository.findById(id).orElse(null);
//        if (subMajor != null) {
//            SubMajorDTO subMajorDTO = new SubMajorDTO();
//            subMajorDTO.setId(subMajor.getId());
//            subMajorDTO.setName(subMajor.getName());
//            subMajorDTO.setField(subMajor.getField());
//            subMajorDTO.setMajorId(subMajor.getMajor().getId());
//            return subMajorDTO;
//        }
//        return null;
//    }

    // update sub major
//    public SubMajorDTO updateSubMajor(SubMajorDTO subMajorDTO) {
//           Long majorId = subMajorDTO.getMajorId();
//            Major major = majorRepository.findById(majorId).orElse(null);
//            if (major == null) {
//                throw new RuntimeException("Major not found");
//            }
//            SubMajor subMajor = subMajorRepository.findById(subMajorDTO.getId()).orElse(null);
//            if (subMajor != null) {
//                subMajor.setName(subMajorDTO.getName());
//                subMajor.setField(subMajorDTO.getField());
//                subMajorRepository.save(subMajor);
//                return subMajorDTO;
//            }
//            return null;
//    }

    // delete sub major
    public void deleteSubMajor(Long id) {
        subMajorRepository.deleteById(id);
    }

}
