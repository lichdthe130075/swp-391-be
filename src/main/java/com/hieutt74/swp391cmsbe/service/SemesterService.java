package com.hieutt74.swp391cmsbe.service;

import com.hieutt74.swp391cmsbe.dto.request.ClassesDTO;
import com.hieutt74.swp391cmsbe.dto.request.SemesterDTO;
import com.hieutt74.swp391cmsbe.dto.response.SemesterResponse;
import com.hieutt74.swp391cmsbe.dto.specification.SemesterSpecification;
import com.hieutt74.swp391cmsbe.models.Classes;
import com.hieutt74.swp391cmsbe.models.Semester;
import com.hieutt74.swp391cmsbe.models.UserEntity;
import com.hieutt74.swp391cmsbe.repository.RoleRepository;
import com.hieutt74.swp391cmsbe.repository.SemesterRepository;
import com.hieutt74.swp391cmsbe.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SemesterService {
    @Autowired
    private SemesterRepository semesterRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;


    private SemesterSpecification semesterSpecification;

    @Transactional(readOnly = true)
    //list all semesters
    public Page<Semester> listAllSemesters(SemesterDTO semesterParams, int page, int size) {
        Pageable pageable = PageRequest.of(page,size);
        Specification<Semester> specification = semesterSpecification.createSpecification(semesterParams);
        Page<Semester> pageResult = semesterRepository.findAll(specification,pageable); // Retrieve a page of data
        return pageResult.map(semester -> modelMapper.map(semester, Semester.class));
    }
    @Transactional(readOnly = true)
    //get semester by id
    public SemesterResponse getSemesterById(Long id) {
        Semester semester = semesterRepository.findById(id).orElse(null);
        if (semester != null) {
            SemesterResponse semesterResponse = new SemesterResponse();
            semesterResponse.setId(semester.getId());
            semesterResponse.setSemesterName(semester.getSemesterName());
            semesterResponse.setYear(semester.getYear());
            semesterResponse.setStartDate(semester.getStartDate());
            semesterResponse.setEndDate(semester.getEndDate());
            if (semester.getCreatedBy() != null) {
                Optional<UserEntity> createdBy = userRepository.findById(semester.getCreatedBy());
                semesterResponse.setCreatedBy(createdBy.get());
            }
            semesterResponse.setCreatedDate(semester.getCreatedDate());

            if (semester.getLastModifiedBy() != null) {
                Optional<UserEntity> lastModifiedBy = userRepository.findById(semester.getLastModifiedBy());
                semesterResponse.setLastModifiedBy(lastModifiedBy.get());
            }

            semesterResponse.setLastModifiedDate(semester.getLastModifiedDate());
            return semesterResponse;
        }
        return null;
    }
    @Transactional
    //create semester
    public Semester createSemester(SemesterDTO semesterDTO) {
        try {
            Semester semester = new Semester();
            LocalDate startDate = LocalDate.parse(semesterDTO.getStartDate());
            LocalDate endDate = LocalDate.parse(semesterDTO.getEndDate());
            semester.setSemesterName(generateSemesterName(startDate));
            semester.setYear(startDate.getYear());
            semester.setStartDate(startDate);
            semester.setEndDate(endDate);
            semester.setCreatedBy(semesterDTO.getCreatedBy());
            semester.setCreatedDate( LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault()));
            return semesterRepository.save(semester);
        }catch (Exception e){
            throw new RuntimeException(e);
        }

    }
    @Transactional
    //update semester
    public Semester updateSemester(Long id,SemesterDTO semesterDTO) {
        Semester semester = semesterRepository.findById(id).orElse(null);
        if (semester != null) {
            LocalDate startDate = LocalDate.parse(semesterDTO.getStartDate());
            LocalDate endDate = LocalDate.parse(semesterDTO.getEndDate());
            semester.setSemesterName(generateSemesterName(startDate));
            semester.setYear(startDate.getYear());
            semester.setStartDate(startDate);
            semester.setEndDate(endDate);
            semester.setLastModifiedBy(semesterDTO.getLastModifiedBy());
            semester.setLastModifiedDate( LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault()));
            semesterRepository.save(semester);
        }
        return null;
    }
    @Transactional
    //delete semester
    public void deleteSemester(Long id) {
        semesterRepository.deleteById(id);
    }


    public Optional<Semester> getCurrentSemester() {
        LocalDate currentDate = LocalDate.now();
        return semesterRepository.findCurrentSemester(currentDate);
    }

    private String generateSemesterName(LocalDate startDate) {
            String semesterName;
        semesterName = switch (startDate.getMonth()) {
            case JANUARY -> "SPRING" + startDate.getYear();
            case MAY -> "SUMMER" + startDate.getYear();
            case SEPTEMBER -> "FALL" + startDate.getYear();
            default -> "";
        };


        return semesterName;
    }
}
