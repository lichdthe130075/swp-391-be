package com.hieutt74.swp391cmsbe.service;

import com.hieutt74.swp391cmsbe.dto.request.CourseDTO;
import com.hieutt74.swp391cmsbe.dto.request.SectionDTO;
import com.hieutt74.swp391cmsbe.dto.response.CourseResponse;
import com.hieutt74.swp391cmsbe.dto.response.SectionResponse;
import com.hieutt74.swp391cmsbe.dto.response.UserResponse;
import com.hieutt74.swp391cmsbe.dto.specification.CourseSpecification;
import com.hieutt74.swp391cmsbe.dto.specification.UserSpecification;
import com.hieutt74.swp391cmsbe.models.Section;
import com.hieutt74.swp391cmsbe.models.UserEntity;
import com.hieutt74.swp391cmsbe.repository.CourseRepository;
import com.hieutt74.swp391cmsbe.repository.SectionRepository;
import com.hieutt74.swp391cmsbe.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.hieutt74.swp391cmsbe.models.Course;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private SectionRepository sectionRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CourseSpecification courseSpecification;


    @Transactional(readOnly = true)
    public Page<Course> getAllCourses(CourseDTO courseParams, int page, int size) throws Exception{
        Pageable pageable = PageRequest.of(page, size);
        Specification<Course> spec = courseSpecification.createSpecification(courseParams);
        Page<Course> pageResult = courseRepository.findAll(spec,pageable); // Retrieve a page of data
        return pageResult.map(course -> modelMapper.map(course, Course.class));
    }

    @Transactional(readOnly = true)
    public CourseResponse getCourseById(Long id) {
        if (courseRepository.existsById(id)){

            Course foundCourse = courseRepository.findById(id).get();

            CourseResponse courseRes = new CourseResponse();
            courseRes.setId(foundCourse.getId());
            courseRes.setCourseCode(foundCourse.getCourseCode());
            courseRes.setCourseName(foundCourse.getCourseName());
            courseRes.setCourseDescription(foundCourse.getCourseDescription());
            courseRes.setStatus(foundCourse.getStatus());

            Set<SectionResponse> set = sectionRepository.findByCourse_Id(courseRes.getId());
            if(!set.isEmpty()) {
                courseRes.setSections(set);
            }

            if (foundCourse.getCreatedBy() != null) {
                Optional<UserEntity> createdByUser = userRepository.findById(foundCourse.getCreatedBy());
                if (createdByUser.isPresent()){
                    UserResponse createdByResp = new UserResponse(createdByUser.get().getUsername(),createdByUser.get().getFirstName(),createdByUser.get().getLastName());
                    courseRes.setCreatedBy(createdByResp);
                }

            }

            courseRes.setCreatedDate(foundCourse.getCreatedDate());

            if (foundCourse.getLastModifiedBy() != null) {
                Optional<UserEntity> lastModifiedByUser = userRepository.findById(foundCourse.getLastModifiedBy());
                if (lastModifiedByUser.isPresent()){
                    UserResponse createdByResp = new UserResponse(lastModifiedByUser.get().getUsername(),lastModifiedByUser.get().getFirstName(),lastModifiedByUser.get().getLastName());
                    courseRes.setLastModifiedBy(createdByResp);
                }
            }
            courseRes.setLastModifiedDate(foundCourse.getLastModifiedDate());
            return courseRes;
        }
        return null;
    }

    @Transactional
    public CourseResponse createCourse(Long instructorId, CourseDTO courseDTO) {
        System.out.println(courseDTO);
        try {
            Course course = new Course();
            if(courseRepository.existsByCourseCode(courseDTO.getCourseCode())){
                throw new Exception("Trùng mã môn");
            }
            course.setCourseCode(courseDTO.getCourseCode());
            course.setCourseName(courseDTO.getCourseName());
            course.setCourseDescription(courseDTO.getCourseDescription());
            course.setStatus(0);
//            course.setStatus(1);
            course.setCreatedBy(instructorId);
            course.setCreatedDate( LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault()));
            Course courseRes =  courseRepository.save(course);
            if(!courseDTO.getSections().isEmpty()){
                List<SectionDTO> sortedSections = courseDTO.getSections()
                        .stream()
                        .sorted(Comparator.comparingInt(SectionDTO::getOrderIndex))
                        .collect(Collectors.toList());

                for (SectionDTO sectionDTO : sortedSections) {
                    Section section = new Section();
                    section.setCourse(courseRes);
                    section.setSectionName(sectionDTO.getSectionName());
                    section.setSectionDescription(sectionDTO.getSectionDescription());
                    section.setSectionOrder(sectionDTO.getOrderIndex());
                    section.setSectionDescription(sectionDTO.getSectionDescription());
                    section.setCreatedBy(instructorId);
                    section.setCreatedDate( LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault()));
                    sectionRepository.save(section);
                }
            }
            return getCourseById(course.getId());
        }catch (Exception ex){
                throw new RuntimeException(ex);
        }
    }

    @Transactional
    public CourseResponse updateCourse(Long instructorId,Long id, CourseDTO courseDTO) {
        try {
            Course course = courseRepository.findById(id).orElseThrow(() -> new RuntimeException("Course not found"));
            course.setCourseName(courseDTO.getCourseName());
            course.setCourseDescription(courseDTO.getCourseDescription());
            course.setLastModifiedBy(instructorId);
            course.setLastModifiedDate( LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault()));
            // Get the existing sections of the course
            Set<Section> existingSections = new HashSet<>();
            for (SectionDTO sectionDTO : courseDTO.getSections()){
                if(sectionDTO.getId() != null){
                    Section tempSection = new Section();
                    tempSection.setId(sectionDTO.getId());
                    tempSection.setSectionName(sectionDTO.getSectionName());
                    tempSection.setSectionDescription(sectionDTO.getSectionDescription());
                    tempSection.setSectionOrder(sectionDTO.getOrderIndex());
                    existingSections.add(tempSection);
                }
            }
            // Create a set to store the updated sections
            Set<Section> updatedSections = new HashSet<>();
            // Iterate through the sections provided in the DTO
            for (SectionDTO sectionDTO : courseDTO.getSections()) {

                // Check if the section has an ID
                if (sectionDTO.getId() != null) {
                    // If the section exists in the database, update it
                    Section existingSection = existingSections.stream()
                            .filter(s -> s.getId().equals(sectionDTO.getId()))
                            .findFirst()
                            .orElseThrow(() -> new RuntimeException("Section not found"));

                    // Update the existing section
                    existingSection.setCourse(course);
                    existingSection.setSectionName(sectionDTO.getSectionName());
                    existingSection.setSectionDescription(sectionDTO.getSectionDescription());
                    existingSection.setSectionOrder(sectionDTO.getOrderIndex());
                    existingSection.setLastModifiedBy(instructorId);
                    existingSection.setLastModifiedDate( LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault()));
                    sectionRepository.save(existingSection);
                    updatedSections.add(sectionRepository.save(existingSection));
                } else {
                    // If the section does not have an ID, create a new section and add it to the course
                    Section newSection = new Section();
                    newSection.setCourse(course);
                    newSection.setSectionName(sectionDTO.getSectionName());
                    newSection.setSectionDescription(sectionDTO.getSectionDescription());
                    newSection.setSectionOrder(sectionDTO.getOrderIndex());
                    newSection.setCreatedBy(instructorId);
                    newSection.setCreatedDate( LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault()));
                    sectionRepository.save(newSection);
                    updatedSections.add(sectionRepository.save(newSection));
                }
            }
            courseRepository.save(course);
            return getCourseById(id);
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    @Transactional
    public void deleteCourse(Long id) {
        try {
            CourseResponse courseRes = getCourseById(id);
            Set<SectionResponse> sectionResponses = sectionRepository.findByCourse_Id(courseRes.getId());
            if(!sectionResponses.isEmpty()) {
                courseRes.setSections(sectionResponses);
                sectionResponses.forEach(sectionResponse -> {
                    sectionRepository.deleteByCourse_Id(courseRes.getId());
                });
            }
            courseRepository.deleteById(courseRes.getId());
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }

    }


    @Transactional
    public void deleteSection(Long id) {
        try {
            sectionRepository.deleteById(id);
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }

    }

    @Transactional
    public CourseResponse changeStatusCourse(Long courseId, CourseDTO courseDTO) {
        try {
            Course course = courseRepository.findById(courseId).orElseThrow(() -> new RuntimeException("Course not found"));
            course.setStatus(courseDTO.getStatus());
            if (courseDTO.getStatus() == 2) {
                course.setManagerFeedback(courseDTO.getManagerFeedback());
            }
            courseRepository.save(course);
            return getCourseById(courseId);
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

}
